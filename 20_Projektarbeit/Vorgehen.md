# Vorgehen

## Datenimport

- Erstellen sie die Tabelle für ihren Datenimput. D.h. sie sieht genau so aus, wie das CSV-File das sie importieren wollen. Dazu erstellen sie dies in Mysql-Workbench ein neues Modell und benennen sie dieses “M164_Projekt”. Dann erstellen sie darin genau eine Tabelle mit allen Feldern (Spalten) die das CSV-File enthält und notieren sie sich das Create-Statement für die Tabelle und Datenbank mit Forward Engineering.
- Erstellen sie ein Mysql-Script in welchem sie die Statements speichern.
[Link zum Load-Into](../10_Auftraege_und_Uebungen/15_Bulkimport/)
- Versuchen sie dieses auszuführen im Mysql Client. Tip: source filename.sql benutzen.

Beispiele für Import:
https://gitlab.com/armindoerzbachtbz/project_opendata_schweine_m164/-/blob/main/scripts/import.sql

## Datenanalyse

- Erstellen sie Tabellen für ihre Auswertung. Bringen sie diese in die 2. Normalform.
- Erkennen der Entitäten in ihrer Import-Tabelle die sie brauchen. Es gibt sehr wahrscheinlich Entitäten, die sie für ihre Auswertung gar nicht brauchen (select distinct … benutzen, um herauszufinden welche Spalten nur einen Wert haben). Diese lassen sie weg.
- Erkennen sie Relationen und Kardinalitäten
- Erstellen sie ein Konzeptionelles, (Logisches) und Physisches Modell. 
- Beim physischen Modell sollten sie darauf achten, dass die Primary Keys und Foreign Keys nach Möglichkeit vom Typ INT sind (dazufügen, wenn es keine solchen Spalten gibt).
- Erstellen sie ein SQL-Script das die Tabellen herstellt.

Beispiele für Modelle finden sie hier:

https://gitlab.com/armindoerzbachtbz/project_opendata_schweine_m164/-/tree/main/models


## Datennormalisierung

- Normalisieren der Daten geschieht mit Statements welche in https://www.w3schools.com/sql/sql_insert_into_select.asp beschrieben sind. Überlegen sie sich, welche Daten sie aus der import-Tabelle wie selektieren und so in die ‘Normalisierten’ Tabellen einfügen. Schreiben sie die dazu nötigen SQL-Statements.

Ein Beispiel SQL-Skript finden sie unter https://gitlab.com/armindoerzbachtbz/project_opendata_schweine_m164/-/blob/main/scripts/normalize_data.sql

## Datenkontrolle - Tests nach Normalisierung

Beim Datenimport und der Normalisierung der Daten kann es vorkommen, dass man Datensätze nicht einfügen kann, weil sie Constraints verletzen. Um auszuschliessen, dass Daten verloren gehen, solltet ihr die Daten in der Import-Tabelle mit den Daten in der Normalisierten Form vergleichen. D.h. ihr schaut, ob ihr gleich viele Datensätze habt, und dass Summen, die ihr bilden könnt, gleich sind.

Ein Beispiel für Tests der Normalisierten Daten: 
https://gitlab.com/armindoerzbachtbz/project_opendata_schweine_m164/-/blob/main/scripts/test.sql

## Datenauswertung
Jede Auswertung wird mit einem SQL-Statement gemacht. Es sollen mehrere Auswertungen in einem Skript zusammengefasst werden.

Beispiele für solche Auswertungen findet ihr in https://gitlab.com/armindoerzbachtbz/project_opendata_schweine_m164/-/blob/main/scripts/auswertungen.sql

