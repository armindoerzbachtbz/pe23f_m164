# 09 Projektauftrag

Ziel des Projektauftrages ist es die während des Unterrichts erlernten Datenbankoperationen zu üben und das Wissen über SQL zu festigen.

Dazu sollen öffentlich zugängliche Daten wie zum Beispiel bei [opendata.swiss](https://opendata.swiss) importiert werden. Diese sollen dann mit SQL sinnvoll umgeformt werden sodass sie dann ausgewertet werden können. Sie können auch andere öffentlich zugängliche Datenquellen brauchen.

# Dauer
Dauer des Projekts: über die ganze Zeit dieses Moduls verteilt ungefähr 20 Stunden + 5 Stunden Hausaufgaben.

# Benotung

Ja nach komplexität des ausgewählten Projektes wird eine Maximalnote erreichbar sein (siehe Beispiele unten), welche ich aufgrund des Anforderungskataloges festlege.

Folgende Kriterien werden einzeln benotet:

| Kriterium                     | Gewichtung |
|-------------------------------|------------|
| Dokumentation                 | 10%        |
| Anforderungskatalog           | 10%        |
| Testbeschreibung              | 10%        |
| Automatisierung des Prozesses | 30%        |
| Auswertung                    | 20%        |
| Geschwindigkeit der Auswertung <br> (Einsatz von Indexen,...)| 20%        |

Pro Kriterium kann höchstens die Maximalnote erreicht werden. 

Eine genauere Beschreibung der Kritereien finden sie im [Bewertungsraster](./Bewertungsraster.md)

# Vorgehen

Eine kurze Beschreibung des Vorgehens finden sie [hier](./Vorgehen.md)

# Mögliche Beispiele

## Einfache (Maximalnote 4.5)

### Beispiel 1 
Wir möchten wissen wie sich die Erwerbsquote über die letzen 3 Jahre gändert hat und was die Unterschiede in einzelnen Bezirken ist.

[https://opendata.swiss/de/dataset/erwerbsquote-nach-bezirk-kumuliert-uber-drei-jahre](https://opendata.swiss/de/dataset/erwerbsquote-nach-bezirk-kumuliert-uber-drei-jahre1)

### Beispiel 2
Wir möchten die Entwicklung des Ausländeranteils an der Wohnbevölkerung darstellen und den Ausländeranteil in den einzelnen Bezirken vergleichen.

[https://opendata.swiss/de/dataset/standige-wohnbevolkerung-kanton-thurgau-ab-2015-nach-bezirken-und-staatsangehorigkeiten](https://opendata.swiss/de/dataset/standige-wohnbevolkerung-kanton-thurgau-ab-2015-nach-bezirken-und-staatsangehorigkeiten)


## Mittlere (Maximalnote 5.5)
Wir möchten das Verkehrsverhalten der Bevölkerung analysieren.
Wir wollen dann die zeitliche Entwicklung der Anteile an zurückgelegten KM der einzelnen Fortbewegungsvarianten auswerten. Auch wollen wir diese nach Grossregionen, Landessprachen auswerten.

**2015:** <br>
[https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen](https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen)

**2021:** <br> 
[https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen1/resource/af2a6062-cc6c-4bfc-8ffd-653abfd0485c](https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen1/resource/af2a6062-cc6c-4bfc-8ffd-653abfd0485c)

## Herausfordernd (Maximalnote 6)
Wir möchten das Verkehrsverhalten der Bevölkerung analysieren und weisen die einzelnen Fortbewegungsarten verschiedenen überkategorien (z.B. low-Carbon, medium-Carbon, high-Carbon) zu, die es so im Datensatz noch nicht gibt. Dann werten wir den Datensatz nach diesen Kategorien aus.
Wir wollen dann die zeitliche Entwicklung der Anteile an zurückgelegten KM der einzelnen Kategorien auswerten. Auch wollen wir diese nach Grossregionen, Landessprachen auswerten.

**2015:** <br>
[https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen](https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen)

**2021:** <br> 
[https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen/resource/52e8a921-f9d2-4ddf-bb05-673157cbb2f3](https://opendata.swiss/de/dataset/verkehrsverhalten-der-bevolkerung-synthesetabellen/resource/52e8a921-f9d2-4ddf-bb05-673157cbb2f3)

## Extrem Herausfordernd (Maximalnote 6.5)
Zwei Datensaetze kombinieren, wie zum Beispiel dieser beiden Datensaetze:

[https://opendata.swiss/de/dataset/aufgeloste-partnerschaften-nach-gegenseitigem-alter-der-partner-und-kanton-2007-2021](https://opendata.swiss/de/dataset/aufgeloste-partnerschaften-nach-gegenseitigem-alter-der-partner-und-kanton-2007-2021)

[https://opendata.swiss/de/dataset/todesfalle-nach-funf-jahres-altersgruppe-geschlecht-und-kanton-1969-2021](https://opendata.swiss/de/dataset/todesfalle-nach-funf-jahres-altersgruppe-geschlecht-und-kanton-1969-2021)

Dabei geht es dann darum, dass Alterskategorien in den beiden Datensätzen nicht gleich sind und angeglichen werden müsssen um die beiden Datensätze zu kombinieren und dann Anzahl Todesfälle in altersklassen mit den aufgelösten Partnerschaften in den Alterklassen zu vergleichen. (Ob das sinnvoll ist weiss man erst wenn man eine solche Auswertung gemacht hat)
Hier wird es dann ziemlich viel SQL-Statements brauchen, um diese Datensätze sinnvoll umzuformen. 

