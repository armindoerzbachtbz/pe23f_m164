# Datensicherung

[TOC]

Zeit: 45 Min.<br>
Form: 2er Team

>Lesen Sie zuerst im Dokument [Skript_M164_Themenübersicht.pdf](../../02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf) das Kapitel mit dem Titel **Datensicherung von Datenbanken**.

In diesem Auftrag erzeugen wir ein logisches Datenbank-Backup mit dem Tool **mysqldump**, u.a. soll Ihre Datenbank **Tourenplaner** gesichert werden.
Mit MariaDB lässensich auch physische Backups im Betrieb erstellen.

### Hier nochmals der Unterschied zwischen logischen und physischen Backups:

Logische Backups bestehen aus den SQL-Anweisungen, die zur Wiederherstellung der Daten erforderlich sind, wie CREATE DATABASE, CREATE TABLE und INSERT.
Physische Backups werden durch Kopieren der einzelnen Datendateien oder Verzeichnisse durchgeführt.

Die Hauptunterschiede sind wie folgt

- Logische Backups sind flexibler, da die Daten auf anderen Hardwarekonfigurationen, MariaDB-Versionen oder sogar auf einem anderen DBMS wiederhergestellt werden können, während physische Backups nicht auf einer deutlich anderen Hardware, einem anderen DBMS oder möglicherweise sogar einer anderen MariaDB-Version eingespielt werden können. (Migration)
- Logische Backups können auf der Ebene von Datenbanken und Tabellen durchgeführt werden, während physische Datenbanken auf der Ebene von Verzeichnissen und Dateien liegen. In den MyISAM- und InnoDB-Speicher-Engines hat jede Tabelle einen entsprechenden Satz von Dateien. 
- Logische Sicherungen sind größer als die entsprechende physische Sicherung.
- Logische Sicherungen benötigen mehr Zeit für die Sicherung und Wiederherstellung als die entsprechende physische Sicherung.


---
## Aufgabe 1: Logisches Backup erstellen

### Backup erstellen mit XAMPP - MySQLDump
Falls Ihr Datenbank mit XAMPP läuft, folgen Sie der Anleitung unter [Generate the backup of a single database](https://www.sqlshack.com/how-to-backup-and-restore-mysql-databases-using-the-mysqldump-command/).

> Hinweis, falls Port vom SQL-Server umgestellt wurde:
> 
```cmd
    C:\XAMPP\MYSQL\BIN\mysqldump -u root -p --port=3307 tourenplaner > C:\BACKUP\tp_dump.sql
```

<br><br>

### Logisches Backup erstellen mit phpMyAdmin
Unter Exportieren haben Sie die Möglichkeit alle oder einzelne DBs des Servers zu sichern.

**Backup des Servers:**
![Backup Server](./x_res/XAMPP-pma-Server.png)

**Backup einer Datenbank:**
![Backup Server](./x_res/XAMPP-pma-Db.png)

Das Dump-File landet im Download-Ordner!

<br><br>

### Logisches Backup erstellen mit DOCKER

In der Container-Technologie werden u.a. Befehle im Container ausgeführt. Ergebnisse (z.B. Files) müssen danach vom Container zum Host (Computer) kopiert werden.

Nachfolgend eine schrittweise Anleitung, wie Sie im Container ein file mit dem Tool «mysqldump» generieren und danach zum Host kopieren.

1.	Legen Sie einen neuen Ordner in ihrem Computer an, z.B. «C:\db_backup».
2.	Öffnen Sie nun ein das Commandline-Tool «cmd».
3.	Führen Sie diese Zeile aus:<br>
    ```bash
    docker exec -it mysql /usr/bin/mysqldump -u root -p --databases tourenplaner --result-file=/var/lib/mysql/tourenplaner_backup.sql
    ```

    Eine Eingabeaufforderung erscheint. Geben Sie das Passwort ein (Gleich wie beim MySql Connection)

    ![](../../x_res/cmd_eingabeaufforderung.png)

    Sobald Sie das Passwort eingegeben haben, wird hier im container das Tool «mysqldump» ausgeführt. Dieses erstellt ein Dump (DDL und DML Befehle der Datenbank) und schreibt diesen ins file «tourenplaner_backup.sql» rein.

4.	Führen Sie als Nächstes diese Zeile aus: <br>
    ```bash
    docker cp mysql:/var/lib/mysql/tourenplaner_backup.sql c:\db_backup\
    ```
    Das file «tourenplaner_backup.sql» wird vom Container zum Host (Computer) kopiert. 

---

## Aufgabe 2: Backup-File analysieren und verifizieren

1.	Ihr dumpfile (Backup-File) ist nun nicht mehr leer. Analysieren Sie den Inhalt.
    - Was finden Sie vor?
    - Beschreiben Sie ihn.
2.	Überprüfen Sie ihr Dump-File (Restore)
    - Indem Sie die Datenbank «tourenplaner» löschen. 
    - Führen Sie nun das Dump-File aus. 
    - Ihre Datenbank sollte wiederhergestellt sein!

    > **Hinweis**: Mit dem Befehl **SOURCE** können Sie das Dump-File direkt einlesen:
    >
    ```sql
    mysql> source C:\MySQLBackup\dump.sql
    ```
    ***Evtl. muss die Datenbasis zuerst erzeugt werden bevor Sie einlesen!!***


## Aufgabe 3: Backup Strategien

1.	Öffnen Sie diese [Seite](https://dev.mysql.com/doc/refman/8.0/en/backup-types.html) und lesen Sie das erste Kapitel **«Physical (Raw) Versus Logical Backups»**<br>
    a.	Welchen Backup haben wir in **Aufgabe 1**  erstellt?<br>
    b.	Welche Nachteile hat dieser Backup?<br>
2.	Was ist der Unterschied zwischen online- und offline Backups?
3.	Was ist ein «snapshot Backup»?


## Aufgabe 4: Physisches Backup (nur MariaDB)

- Erstellen Sie ein Full-Backup mit dem Backup-Programm mariabackup.exe. 
- Wozu muss die Backupdatenstruktur vor einem Restore "vorbereitet" (prepare) werden?
- Wie heist der Parameter, um ein Restore zu machen?
- Ist ein inkrementelles und differentielles Backup möglich? Wenn ja wie?

[Mariabackup Manual](https://mariadb.com/kb/en/mariabackup-overview/#using-mariabackup)

## Aufgabe 5: Backup mit externem Backup-Programm

Studieren Sie folgende Anleitung für Backup und Restore: [Backup mit Acronis](https://www.acronis.com/de-de/blog/posts/mysql-backup/)

---
Quellen: <br>
https://dev.mysql.com/doc/refman/8.0/en/mysqldump.html
