# Datensicherung

[TOC]

Zeit: 45 Min.<br>
Form: 2er Team

>Lesen Sie zuerst im Dokument [Skript_M164_Themenübersicht.pdf](../../02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf) das Kapitel mit dem Titel **Datensicherung von Datenbanken**.

In diesem Auftrag erzeugen wir ein logisches Datenbank-Backup mit dem Tool **mysqldump**, u.a. soll Ihre Datenbank **Tourenplaner** gesichert werden.
Mit MariaDB lässensich auch physische Backups im Betrieb erstellen.

### Der Unterschied zwischen logischen und physischen Backups:

Logische Backups bestehen aus den SQL-Anweisungen, die zur Wiederherstellung der Daten erforderlich sind, wie CREATE DATABASE, CREATE TABLE und INSERT.
Physische Backups werden durch Kopieren der einzelnen Datendateien oder Verzeichnisse durchgeführt.

Die Hauptunterschiede sind wie folgt

- Logische Backups sind flexibler, da die Daten auf anderen Hardwarekonfigurationen, MariaDB-Versionen oder sogar auf einem anderen DBMS wiederhergestellt werden können, während physische Backups nicht auf einer deutlich anderen Hardware, einem anderen DBMS oder möglicherweise sogar einer anderen MariaDB-Version eingespielt werden können. (Migration)
- Logische Backups können auf der Ebene von Datenbanken und Tabellen durchgeführt werden, während physische Datenbanken auf der Ebene von Verzeichnissen und Dateien liegen. In den MyISAM- und InnoDB-Speicher-Engines hat jede Tabelle einen entsprechenden Satz von Dateien. 
- Logische Sicherungen sind größer als die entsprechende physische Sicherung.
- Logische Sicherungen benötigen mehr Zeit für die Sicherung und Wiederherstellung als die entsprechende physische Sicherung.


---
## Aufgabe 1: Logisches Backup erstellen

### Backup erstellen mit MySQLDump
Finden Sie raus wo auf Ihrer Umgebung das Tool mysqldump liegt. Dies variiert, abhängig von ihrer Installation. Erstellen Sie ein dump Ihrer Datenbank *Tourenplaner*. Weitere Infos zu mysqldump: <https://dev.mysql.com/doc/refman/8.0/en/mysqldump.html>

Stellen Sie sicher, dass Sie folgende Optionen/Parameter aktiviert haben:

- Drop-Statements sollen eingefügt sein
- Fügen Sie Kommentare zum Backup hinzu
- Das Datum soll ebenfalls in die Datei geschrieben werden.



---

## Aufgabe 2: Backup-File analysieren und verifizieren

1.	Analysieren Sie den Inhalt der Dump-Datei
    - Was finden Sie vor?
    - Beschreiben Sie ihn.
2.	Überprüfen Sie ihr Dump-File (Restore)
    - Indem Sie die exportierte Datenbank löschen. 
    - Führen Sie nun das Dump-File aus. 
    - Ihre Datenbank sollte wiederhergestellt sein!

    > **Hinweis**: Mit dem Befehl **SOURCE** können Sie das Dump-File direkt einlesen:
    ```sql
    mysql> source C:\MySQLBackup\dump.sql
    ```



## Aufgabe 3: Backup Strategien

1.	Öffnen Sie diese [Seite](https://dev.mysql.com/doc/refman/8.0/en/backup-types.html) und lesen Sie das erste Kapitel **«Physical (Raw) Versus Logical Backups»**<br>
   a.	Welchen Backup haben wir in **Aufgabe 1**  erstellt?<br>
   b.	Welche Nachteile hat dieser Backup?<br>
2.	Was ist der Unterschied zwischen online- und offline Backups?
3.	Was ist ein «snapshot Backup»?




## Aufgabe 4: Physisches Backup (nur MariaDB)

- Erstellen Sie ein Full-Backup mit dem Backup-Programm mariabackup.exe. 
- Wozu muss die Backupdatenstruktur vor einem Restore "vorbereitet" (prepare) werden?
- Wie heisst der Parameter, um ein Restore zu machen?
- Ist ein inkrementelles und differentielles Backup möglich? Wenn ja wie?

[Mariabackup Manual](https://mariadb.com/kb/en/mariabackup-overview/#using-mariabackup)

