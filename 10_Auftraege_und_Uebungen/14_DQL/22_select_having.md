# Select having

Gegeben ist die [schuleDatenbank](../90_Dumps/schuleDatenbank_dump.sql)

![](../../x_res/SchuleDatenbankERD.png)


Zeit: 30 Min.<br>
Form: 2er Team

## Aufgaben
1.	<br>
    a.	Geben Sie eine Liste der Durchschnittsnoten (Deutsch, Mathe) aller Schüler aus; es werden aber nur die Schüler ausgegeben, deren Durchschnitt besser als 4 ist. Ausgabe: Schülername, Durchschnittsnote<br>
    b.	Runden Sie in der vorigen Aufgabe die Durchschnittsnote auf eine Dezimale und sortieren Sie die Ausgabe nach der Durchschnittsnote aufsteigend.<br>
2.	Geben Sie eine Liste aller Lehrer und ihres Nettogehalts (Gehalt * 0.7) aus. Wir wollen nur die Lehrer sehen, deren Nettogehalt mehr als 3000 Euro beträgt.
3.	Wir wollen herausbekommen, in welchen Klassenzimmern zu wenig Schüler unterrichtet werden. Geben Sie eine Liste der Klassenzimmer und die in diesen Klassenzimmern unterrichteten Schüler aus (Spalten "Klassenzimmer", "Anzahl"). Dabei interessieren uns nur die Klassenzimmer, in denen weniger als 10 Schüler sitzen.
4.	<br>
    a.	Wie viele Schüler mit russischer Herkunft (Nationalität: "RU") wohnen in den einzelnen Orten? Geben Sie eine Liste aus mit "Anzahl" und "Ort-Name". Bitte nach Ort-Name aufsteigend sortieren.<br>
    b.	Erweitern Sie die Aufgabe 4.a so, dass nur die Orte ausgegeben werden, in denen 10 oder mehr russischstämmige Schüler wohnen.<br>
