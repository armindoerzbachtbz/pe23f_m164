# Select alias

Verwenden Sie die [kundenDatenbank](../90_Dumps/kundenDatenbank_dump.sql).

Zeit: 30 Min.<br>
Form: 2er Team

## Aufgaben

1.	Ergänzen Sie folgende Statements und überprüfen Sie:

    ```sql
    SELECT ___________________  
      FROM kunden AS kundenliste 
      WHERE kundenliste.fk_ort_postleitzahl > 80000
    ```
    Ausgegeben werden sollen kunde_id, Name des Kunden und Postleitzahl des Kunden.


    Ergänzen Sie:
    
    ```sql
    SELECT o.name, k.name, 
      FROM _________ AS __________ INNER JOIN ________ AS ___________ 
      ON _________ = __________
      WHERE o.name LIKE '%n'
    ```


2.	Korrigieren Sie die folgenden Statements, dass sie funktionieren:

    ```sql
    -- Aliasse 'prfz' und 'hrgs' bitte nicht verändern!
    
    SELECT kunde_id, kunden.name, ort.name 
      FROM kunden AS hrgs INNER JOIN ort AS prfz 
      ON o.id_postleitzahl = k.fk_ort_postleitzahl 
      ORDER BY k.kunde_id
    ```

    ```sql
    -- Fügen Sie die notwendigen Aliasse ein!
    
    SELECT k.name, o.postleitzahl ,o.name FROM kunden
    INNER JOIN ort ON k.fk_ort_postleitzahl = o.id_postleitzahl
    WHERE k.name LIKE '%a%' 
    AND o.name LIKE '%u%'
    ```

    > Hinweis: Ein eigeführter Alias müss ÜBERALL verwendet werden!
