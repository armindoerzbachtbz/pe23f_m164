# Select group by

Gegeben ist die [schuleDatenbank](../90_Dumps/schuleDatenbank_dump.sql)

![](../../x_res/SchuleDatenbankERD.png)

Zeit: 30 Min.<br>
Form: 2er Team

## Aufgaben
Setzen Sie diese Aufgabe um: 

a.	Geben Sie die Anzahl aller Schüler aus, gruppiert nach Nationalität (Spalten: "Anzahl", "Nationalität").<br>
b.	Wie viele Schüler wohnen in den einzelnen Orten? Ausgabe: "Ort", "Anzahl der Schüler" (bitte genauso), sortiert nach Anzahl der Schüler absteigend<br>
c.	Erstellen Sie eine Liste, aus der ersichtlich wird, wie viele Lehrer die einzelnen Fächer unterrichten, sortiert nach Anzahl absteigend. Ausgabe: Fachbezeichnung, Anzahl<br>
d.	Erstellen Sie eine Liste, aus der ersichtlich wird, welche Lehrer die jeweiligen Fächer unterrichten, sortiert nach Anzahl der Lehrer absteigend. Pro Fach bitte nur eine Zeile! Ausgabe: Fachbezeichnung, Lehrerliste (bitte KEINE Spalte, in der die Anzahl der Lehrer steht).<br>
e.	Wir brauchen eine Liste, die die Schülernamen auflistet und die Fächer, in denen diese Schüler unterrichtet werden. Ausgabe: "Schülername", "Lehrer", "Fächer"<br>
