# Aggregatsfunktionen

Gegeben ist die [schuleDatenbank](../90_Dumps/schuleDatenbank_dump.sql)

![](../../x_res/SchuleDatenbankERD.png)

Zeit: 30 Min.<br>
Form: 2er Team

## Aufgaben
Setzen Sie diese Aufgaben um: 

a.	Welches ist das niedrigste/höchste Gehalt eines Lehrers?<br>
b.	Was ist das niedrigste Gehalt, das einer unserer Mathelehrer bekommt?<br>
c.	Was ist der beste Notendurchschnitt der Noten Deutsch/Mathe?<br>
d.	Wie viel Einwohner hat der größte Ort, wie viel der Kleinste? Ausgabe "Höchste Einwohnerzahl", "Niedrigste Einwohnerzahl"<br>
e.	Wie groß ist die Differenz zwischen dem Ort mit den meisten und dem mit den wenigsten Einwohnern (z.B.: kleinster Ort hat 1000 Einwohner, größter Ort hat 3000 - Differenz ist 2000). Ausgabe einer Spalte "Differenz".<br>
f.	Wie viele Schüler haben wir in der Datenbank?<br>
g.	Wie viele Schüler haben ein Smartphone?<br>
h.	Wie viele Schüler haben ein Smartphone der Firma Samsung oder der Firma HTC?<br>
i.	Wie viele Schüler wohnen in Waldkirch?<br>
j.	Wie viele Schüler, die bei Herrn Bohnert Unterricht haben, wohnen in Emmendingen?<br>
k.	Wie viele Schüler unterrichtet Frau Zelawat?<br>
l.	Wie viele Schüler russischer Nationalität unterrichtet Frau Zelawat?<br>
m.	Welcher Lehrer verdient am meisten? (Achtung: Falle! Überprüfen Sie Ihr Ergebnis.)<br>
