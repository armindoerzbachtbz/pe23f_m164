# DB Mondial

[TOC]

Zeit: 45 Min.<br>
Form: 2er Team

Beiliegend finden Sie [das Skript für die Datenstruktur](MYSQl/mondial-schema-mysql.sql) sowie das [Daten-Import Skript](MYSQl/mondial-inputs-mysql.sql) von DB Mondial.

Analysieren Sie die beiden files.

## Wissensfragen

1. Grundätzlich funktioniert die [Datenstruktur](MYSQl/mondial-schema-mysql.sql). Aber etwas Wichtiges fehlt in den DDL-Statements! Was?
2. Was sind die Konsequenzen, wenn man diese Datenbank trotzdem weiter betreibt?


## Query Aufträge
Wie schon erwähnt, funktioniert diese Datenbank, ohne die wichtige Ergänzung in den DDL-Statements, einwandfrei.
Schreiben Sie die Queries für die nachfolgenden Fragen:

1.	Durch welche Länder fließt die Donau?

2.	Alle Länder, durch die ein Fluss fließt, der in den Atlantik mündet.

3.	Welche Länder liegen in Asien?

4.	Wieviel Nachbarländer hat Deutschland?

5.	Welche Nachbarländer hat Deutschland?

6.	Zeige die Bundesstaaten der USA!

7.	Zeige den höchsten Berg von jedem Kontinent!

8.	Wieviel Prozent haben die verschiedenen Religionen in Deutschland?

9.	Zeige die Hauptstädte der deutschen Bundesländer!

10.	Zeige Ländername und Kindersterblichkeit der Länder in Europa, geordnet nach
Kindersterblichkeit.

11.	Welche Länder gehören zur EU?
