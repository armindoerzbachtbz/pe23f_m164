# HR Datenbank (Aggregatsfunktionen)

[TOC]

Zeit: 90 Min.<br>
Form: 2er Team

Gegeben ist die [Datenbank humanresources](humanresources.mwb)

## Aufgaben

1.	Schreiben Sie eine Abfrage, um die Anzahl der verfügbaren Jobs in der Tabelle „Employees“ aufzulisten.


2.	Schreiben Sie eine Abfrage, um die an die Mitarbeiter zu zahlenden Gesamtgehälter zu erhalten.

3.	Schreiben Sie eine Abfrage, um das kleinste Gehalt aus der Mitarbeitertabelle abzurufen.

4.	Schreiben Sie eine Abfrage, um das maximale Gehalt eines Mitarbeiters zu erhalten, der als Programmierer arbeitet.

5.	Schreiben Sie eine Abfrage, um das durchschnittliche Gehalt und die Anzahl der Mitarbeiter in der Abteilung 90 zu erhalten.

6.	Schreiben Sie eine Abfrage, um das höchste, niedrigste, summierte und durchschnittliche Gehalt aller Mitarbeiter zu erhalten.

7.	Schreiben Sie eine Abfrage, um die Anzahl der Mitarbeiter mit demselben Job zu erhalten.

8.	Schreiben Sie eine Abfrage, um die Differenz zwischen dem höchsten und dem niedrigsten Gehalt zu erhalten.

9.	Schreiben Sie eine Abfrage, um die Manager-ID und das Gehalt des am niedrigsten bezahlten Mitarbeiters für diesen Manager zu finden.

10.	 Schreiben Sie eine Abfrage, um die Abteilungs-ID und das in jeder Abteilung zu zahlende Gesamtgehalt zu erhalten.

11. Schreiben Sie eine Abfrage, um das durchschnittliche Gehalt für jede Job-ID ohne Programmierer zu erhalten.

12. Schreiben Sie eine Abfrage, um das Gesamtgehalt, das Maximum, das Minimum und das Durchschnittsgehalt der Mitarbeiter (in Bezug auf die Job-ID) nur für die Abteilungs-ID 90 zu erhalten.

13. Schreiben Sie eine Abfrage, um die Job-ID und das maximale Gehalt der Mitarbeiter zu erhalten, bei denen das maximale Gehalt größer oder gleich 4000 US-Dollar ist.

14. Schreiben Sie eine Abfrage, um das durchschnittliche Gehalt für alle Abteilungen mit mehr als 10 Mitarbeitern zu erhalten.

