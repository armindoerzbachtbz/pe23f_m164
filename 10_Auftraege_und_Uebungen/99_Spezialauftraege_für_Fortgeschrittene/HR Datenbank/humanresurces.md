# HR Datenbank

[TOC]

Zeit: 90 Min.<br>
Form: 2er Team

Gegeben ist die [Datenbank humanresources](humanresources.mwb)

Führen Sie die beiden Skripte in der Reihenfolge aus:
1.	[hr_database_structure.sql](hr_database_structure.sql) (Datenmodell in der DB erstellen)
2.	[hr_database_data.sql](hr_database_data.sql) (Daten in die Datenbank hinzufügen)

## Aufgaben: Teil 1

1.	Etwas ist bei der Entwicklung des Entity Relationship Diagram's schiefgelaufen. 
    - Beschreiben Sie den Fehler.
    - Theoretisch könnten Sie, ohne den Fehler zu korrigieren weiterarbeiten. Welche Gefahr besteht, wenn Sie trotz ohne Korrektur weiterarbeiten?

2.	Sie haben einen potenziellen Fehler gefunden. Korrigieren das Entity Relationship Diagramm sowie das entsprechende Skript und erstellen Sie die Datenbank erneut.

## Aufgaben: Teil 2

Nun ist die Datenbank bereit. Setzen Sie die nachfolgenden Anforderungen um.

1. Schreiben Sie eine Abfrage, um die employee-ID, den Namen (first_name, last_name) und das Gehalt in aufsteigender Reihenfolge des Gehalts zu erhalten.
2. Schreiben Sie eine Abfrage, um das maximale und minimale Gehalt aus der Mitarbeiter Tabelle zu erhalten.
3. Schreiben Sie eine Abfrage, um die Anzahl der Mitarbeiter zu erhalten, die für das Unternehmen arbeiten
4. Schreiben Sie eine Abfrage, die im Nachnamen von Mitarbeitern mit „R“ anfängt und mit „s“ endet.
5. Listen Sie Namen auf, welche zurzeit keine Anstellung haben.
6. Schreiben Sie eine Abfrage, um die Namen (first_name, last_name) und das Gehalt aller Mitarbeiter anzuzeigen, deren Gehalt nicht zwischen 10.000 und 15.000 US-Dollar liegt.

7. Schreiben Sie eine Abfrage, um den Namen (first_name, last_name) und die department-ID aller Mitarbeiter in den Abteilungen 30 oder 100 in aufsteigender Reihenfolge anzuzeigen. 

8. Schreiben Sie eine Abfrage, um den Namen (first_name, last_name) und das Gehalt aller Mitarbeiter anzuzeigen, deren Gehalt nicht zwischen 10.000 und 15.000 US-Dollar liegt und die in Abteilung 30 oder 100 sind.

9. Schreiben Sie eine Abfrage, um den Namen (first_name, last_name) und das Einstellungsdatum für alle Mitarbeiter anzuzeigen, die 1987 eingestellt wurden.

10. Schreiben Sie eine Abfrage, um den Vornamen aller Mitarbeiter anzuzeigen, die sowohl "b" als auch "c" in ihrem Vornamen haben.

11. Schreiben Sie eine Abfrage, um den Nachnamen, den Job und das Gehalt aller Mitarbeiter anzuzeigen, deren Job der eines Programmierers oder eines Versandmitarbeiters ist und deren Gehalt nicht 4.500 $, 10.000 $ oder 15.000 $ entspricht.

12. Schreiben Sie eine Abfrage, die im Nachnamen von Mitarbeitern „e“ als drittes Zeichen enthält.

13. Schreiben Sie eine Abfrage, um die verfügbaren Stellen/Bezeichnungen (jobs/designations) in der Mitarbeitertabelle anzuzeigen.

14. Schreiben Sie eine Abfrage, um den Namen (Vorname, Nachname), das Gehalt und die Gewinnbeteiligung (15 % des Gehalts) aller Mitarbeiter anzuzeigen.

15. Schreiben Sie eine Abfrage, um alle Datensätze von Mitarbeitern auszuwählen, deren Nachnamen „JONES“, „BLAKE“, „SCOTT“, „KING“ und „FORD“ sind.

16. Schreiben Sie eine Abfrage, um den Namen (Vorname, Nachname), die Stelle, die Abteilungs-ID und den Namen der Mitarbeiter zu finden, die in London arbeiten.

17. Schreiben Sie eine Abfrage, um die Mitarbeiter-ID, den Namen (Nachname) zusammen mit der Manager-ID und dem Namen (Nachname) zu finden.

18. Schreiben Sie eine Abfrage, um den Namen (Vorname, Nachname) und das Einstellungsdatum der Mitarbeiter zu finden, die nach „Jones“ eingestellt wurden.

19. Schreiben Sie eine Abfrage, um die Mitarbeiter-ID, die Berufsbezeichnung, die Anzahl der Tage zwischen dem Enddatum und dem Startdatum für alle Jobs in Abteilung 90 aus dem Jobverlauf zu finden.

20. Schreiben Sie eine Abfrage, um die Abteilungs-ID und den Namen und Vornamen des Managers anzuzeigen.

21. Schreiben Sie eine Abfrage, um den Abteilungsnamen, den Managernamen und die Stadt anzuzeigen.

22. Schreiben Sie eine Abfrage, um die Berufsbezeichnung und das Durchschnittsgehalt der Mitarbeiter anzuzeigen.

23. Schreiben Sie eine Abfrage, um die Berufsbezeichnung, den Namen des Mitarbeiters und die Differenz zwischen dem Gehalt des Mitarbeiters und dem Mindestgehalt für den Job anzuzeigen.

24. Schreiben Sie eine Abfrage, um den job history anzuzeigen. Zwar von einem Mitarbeiter, der derzeit mehr als 10000 Gehalt bezieht.

25. Schreiben Sie eine Abfrage, um den Vornamen, den Nachnamen, das Einstellungsdatum und das Gehalt des Managers für alle Manager anzuzeigen, deren Erfahrung mehr als 15 Jahre beträgt.
