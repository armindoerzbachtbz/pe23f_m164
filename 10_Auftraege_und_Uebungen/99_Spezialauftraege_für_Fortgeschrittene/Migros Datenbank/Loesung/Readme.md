# Vorgehen zur Umsetzung der Lösung

1. Alle Stammdaten (Artikel und Filiale) mit Bulkimport importieren
2. Sortiment pro Kassabon hinzufügen gem. Excel generierte SQL Statement. Achtung INSERT IGNORE verwenden, um Duplicate Row Fehlermeldung zu ignorieren.
3. Kassabon hinzufügen. 1 Row pro Einkauf bzw. Kassabon
4. Verkaufsdetail hinzufügen gem. Excel generierte SQL Statement

## Kassabon 1, receipts-details (1).csv

```sql
--Filiale ermitteln
SELECT @filiale:=PK_FilialeId FROM Filiale WHERE Filialname='M Jona';
```

```sql
---Sortiment hinzufügen
SELECT @artikel:=PK_ArtikelId FROM Artikel WHERE Artikelname='Schweinshalssteak' AND Preis=16;
INSERT IGNORE INTO SORTIMENT VALUES(@artikel,@filiale);
SELECT @artikel:=PK_ArtikelId FROM Artikel WHERE Artikelname='Alpenbrot' AND Preis=2.5;
INSERT IGNORE INTO SORTIMENT VALUES(@artikel,@filiale);
```

```sql
--Kassabon hinzufügen
INSERT IGNORE INTO Kassabon VALUES(null,71,15.06.2022 17:40:04,256,@filiale);
```

```sql
--Verkaufsdetail hinzufügen
INSERT IGNORE INTO Verkaufsdetail VALUES (null,@artikel,@kassabon,0.847);
INSERT IGNORE INTO Verkaufsdetail VALUES (null,@artikel,@kassabon,2);
```



