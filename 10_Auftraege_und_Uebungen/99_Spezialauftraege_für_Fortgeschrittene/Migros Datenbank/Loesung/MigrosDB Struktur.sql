-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema MigrosDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema MigrosDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `MigrosDB` DEFAULT CHARACTER SET utf8 ;
USE `MigrosDB` ;

-- -----------------------------------------------------
-- Table `MigrosDB`.`Cumuluskunde`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MigrosDB`.`Cumuluskunde` (
  `PK_CumuluskundeId` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Vorname` VARCHAR(45) NULL,
  `Cumulusnummer` INT NULL,
  `Cumuluskundecol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`PK_CumuluskundeId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MigrosDB`.`Filiale`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MigrosDB`.`Filiale` (
  `PK_FilialeId` INT NOT NULL,
  `Filialname` VARCHAR(45) NULL,
  PRIMARY KEY (`PK_FilialeId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MigrosDB`.`Kassabon`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MigrosDB`.`Kassabon` (
  `PK_KassabonId` INT NOT NULL,
  `Transaktionsnummer` INT NULL,
  `Einkaufsdatum` DATETIME NULL,
  `Kassennummer` INT NULL,
  `FK_FilialeId` INT NOT NULL,
  `FK_CumuluskundeId` INT NULL,
  PRIMARY KEY (`PK_KassabonId`),
  INDEX `fk_Kassabon_Filiale1_idx` (`FK_FilialeId` ASC) VISIBLE,
  INDEX `fk_Kassabon_Cumuluskunde1_idx` (`FK_CumuluskundeId` ASC) VISIBLE,
  CONSTRAINT `fk_Kassabon_Filiale1`
    FOREIGN KEY (`FK_FilialeId`)
    REFERENCES `MigrosDB`.`Filiale` (`PK_FilialeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Kassabon_Cumuluskunde1`
    FOREIGN KEY (`FK_CumuluskundeId`)
    REFERENCES `MigrosDB`.`Cumuluskunde` (`PK_CumuluskundeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MigrosDB`.`Artikel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MigrosDB`.`Artikel` (
  `PK_ArtikelId` INT NOT NULL,
  `Artikelname` VARCHAR(45) NOT NULL,
  `Preis` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`PK_ArtikelId`),
  UNIQUE INDEX `Artikelname_Preis_UNIQUE` (`Artikelname` ASC, `Preis` ASC) INVISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MigrosDB`.`Verkaufsdetail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MigrosDB`.`Verkaufsdetail` (
  `Menge` INT NOT NULL,
  `FK_ArtikelId` INT NOT NULL,
  `FK_KassabonId` INT NOT NULL,
  INDEX `fk_Verkaufsdetail_Artikel1_idx` (`FK_ArtikelId` ASC) VISIBLE,
  INDEX `fk_Verkaufsdetail_Kassabon1_idx` (`FK_KassabonId` ASC) VISIBLE,
  PRIMARY KEY (`FK_ArtikelId`, `FK_KassabonId`),
  CONSTRAINT `fk_Verkaufsdetail_Artikel1`
    FOREIGN KEY (`FK_ArtikelId`)
    REFERENCES `MigrosDB`.`Artikel` (`PK_ArtikelId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Verkaufsdetail_Kassabon1`
    FOREIGN KEY (`FK_KassabonId`)
    REFERENCES `MigrosDB`.`Kassabon` (`PK_KassabonId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MigrosDB`.`Sortiment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MigrosDB`.`Sortiment` (
  `FK_ArtikelId` INT NOT NULL,
  `FK_FilialeId` INT NOT NULL,
  PRIMARY KEY (`FK_ArtikelId`, `FK_FilialeId`),
  INDEX `fk_Sortiment_Filiale1_idx` (`FK_FilialeId` ASC) VISIBLE,
  CONSTRAINT `fk_Sortiment_Artikel`
    FOREIGN KEY (`FK_ArtikelId`)
    REFERENCES `MigrosDB`.`Artikel` (`PK_ArtikelId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Sortiment_Filiale1`
    FOREIGN KEY (`FK_FilialeId`)
    REFERENCES `MigrosDB`.`Filiale` (`PK_FilialeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
