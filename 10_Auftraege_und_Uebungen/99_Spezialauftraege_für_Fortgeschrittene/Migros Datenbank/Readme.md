# Migros Datenbank

Gegeben sind ein konzeptionelles Datenmodell sowie einige Kassenbons aus verschiedenen Einkäufen.

![](cumulus20200622115555.png)


Zeit: 135 Min.<br>
Form: 2er Team

## Auftrag 

**Bilden Sie das Kassasystem des Migros Detailhändlers nach. Setzen Sie die nachfolgenden Aufgaben um.**


1.	Erstellen Sie ein entsprechendes Datenmodell für die logische Entwurfsebene in chen-Notation.

2.	Physische Entwurfsebene:

    a. Erstellen Sie ein entsprechendes Datenmodell in Krähenfuss-Notation. Erstellen Sie das Modell im mySql-workbench.
    b. Erstellen Sie für jede Tabelle ein DDL-Skript. Stellen Sie sicher, dass die *Datenintegrität* eingehalten wird.
    c. Erstellen Sie die Migros-Datenbank und führen Sie ihre DDL-Skripts aus.

3.	Fügen Sie die [Kassenbons-Daten](Kassenbons/) in ihre Migros-Datenbank hinzu. Erstellen Sie dazu DML-Skripts.

    Hinweis: Nur «MMM» Filialen führen sämtliche Artikel im Sortiment. Alle andere Filialen führen nur so viel Artikel im Sortiment, wie sie am Kassasystem erfasst wurden.

4. Jedes Kassenbon soll anhand eines Queries nachgebildet werden. Schreiben Sie die jeweiligen Queries und legen Sie diese in einem eigenen SQL-Skript file ab.

5. Welche Filiale hat den grössten/ niedrigsten Umsatz gemacht?
