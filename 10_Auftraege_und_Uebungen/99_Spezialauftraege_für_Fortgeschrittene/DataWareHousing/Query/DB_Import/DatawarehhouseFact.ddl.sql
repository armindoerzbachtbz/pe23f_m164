-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 17. Jan 2015 um 07:53
-- Server Version: 5.6.16
-- PHP-Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `DataWarehouse`
--
CREATE DATABASE IF NOT EXISTS `DataWarehouse` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `DataWarehouse`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_artikel`
--

DROP TABLE IF EXISTS `tbl_artikel`;
CREATE TABLE IF NOT EXISTS `tbl_artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Artikel` varchar(50) NOT NULL,
  `Beschreibung` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `tbl_artikel`
--

INSERT INTO `tbl_artikel` (`id`, `Artikel`, `Beschreibung`) VALUES
(1, 'Herrenschuhe', ''),
(2, 'Damenschuhe', ''),
(3, 'Kinderschuhe', ''),
(4, 'Pflegemittel', ''),
(5, 'Zubehör', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_fact`
--

DROP TABLE IF EXISTS `tbl_fact`;
CREATE TABLE IF NOT EXISTS `tbl_fact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Anzahl` int(11) NOT NULL,
  `Umsatz` double NOT NULL,
  `Gewinn` double NOT NULL,
  `fk_artikel` int(11) NOT NULL,
  `fk_filiale` int(11) NOT NULL,
  `fk_quartal` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=728 ;

--
-- Daten für Tabelle `tbl_fact`
--

INSERT INTO `tbl_fact` (`id`, `Anzahl`, `Umsatz`, `Gewinn`, `fk_artikel`, `fk_filiale`, `fk_quartal`) VALUES
(601, 32, 3155.2, 1009.66, 1, 2, 5),
(602, 37, 3648.2, 1167.42, 1, 6, 4),
(603, 45, 4437, 1419.84, 1, 2, 3),
(604, 53, 5225.8, 1672.26, 1, 6, 1),
(605, 57, 5620.2, 1798.46, 1, 5, 3),
(606, 67, 6606.2, 2113.98, 1, 1, 5),
(607, 66, 6507.6, 2082.43, 1, 3, 5),
(608, 37, 3648.2, 1167.42, 1, 1, 1),
(609, 47, 4634.2, 1482.94, 1, 3, 3),
(610, 52, 5127.2, 1640.7, 1, 2, 4),
(611, 70, 6902, 2208.64, 1, 6, 5),
(612, 30, 2958, 946.56, 1, 6, 3),
(613, 52, 5127.2, 1640.7, 1, 4, 4),
(614, 53, 5225.8, 1672.26, 1, 4, 5),
(615, 53, 5225.8, 1672.26, 1, 1, 3),
(616, 67, 6606.2, 2113.98, 1, 2, 1),
(617, 32, 3155.2, 1009.66, 1, 5, 1),
(618, 53, 5225.8, 1672.26, 1, 5, 4),
(619, 33, 3253.8, 1041.22, 1, 4, 3),
(620, 43, 4239.8, 1356.74, 1, 5, 5),
(621, 67, 6606.2, 2113.98, 1, 3, 4),
(622, 34, 3352.4, 1072.77, 1, 4, 1),
(623, 48, 4732.8, 1514.5, 1, 1, 4),
(624, 67, 6606.2, 2113.98, 1, 3, 1),
(625, 32, 3600, 1260, 2, 4, 5),
(626, 36, 4050, 1417.5, 2, 3, 3),
(627, 62, 6975, 2441.25, 2, 6, 4),
(628, 68, 7650, 2677.5, 2, 4, 4),
(629, 30, 3375, 1181.25, 2, 6, 5),
(630, 58, 6525, 2283.75, 2, 5, 3),
(631, 58, 6525, 2283.75, 2, 1, 1),
(632, 57, 6412.5, 2244.38, 2, 5, 1),
(633, 61, 6862.5, 2401.88, 2, 2, 1),
(634, 64, 7200, 2520, 2, 2, 5),
(635, 68, 7650, 2677.5, 2, 1, 5),
(636, 43, 4837.5, 1693.13, 2, 6, 1),
(637, 48, 5400, 1890, 2, 3, 5),
(638, 57, 6412.5, 2244.38, 2, 2, 4),
(639, 51, 5737.5, 2008.13, 2, 5, 4),
(640, 32, 3600, 1260, 2, 3, 1),
(641, 33, 3712.5, 1299.38, 2, 6, 3),
(642, 42, 4725, 1653.75, 2, 3, 4),
(643, 47, 5287.5, 1850.63, 2, 5, 5),
(644, 47, 5287.5, 1850.63, 2, 1, 4),
(645, 63, 7087.5, 2480.63, 2, 4, 3),
(646, 60, 6750, 2362.5, 2, 4, 1),
(647, 60, 6750, 2362.5, 2, 2, 3),
(648, 61, 6862.5, 2401.88, 2, 1, 3),
(649, 36, 2728.8, 791.35, 3, 2, 1),
(650, 39, 2956.2, 857.3, 3, 6, 1),
(651, 52, 3941.6, 1143.06, 3, 1, 1),
(652, 41, 3107.8, 901.26, 3, 5, 3),
(653, 41, 3107.8, 901.26, 3, 4, 1),
(654, 45, 3411, 989.19, 3, 3, 3),
(655, 49, 3714.2, 1077.12, 3, 5, 1),
(656, 61, 4623.8, 1340.9, 3, 6, 4),
(657, 67, 5078.6, 1472.79, 3, 2, 5),
(658, 40, 3032, 879.28, 3, 6, 3),
(659, 48, 3638.4, 1055.14, 3, 5, 4),
(660, 57, 4320.6, 1252.97, 3, 1, 5),
(661, 64, 4851.2, 1406.85, 3, 3, 5),
(662, 45, 3411, 989.19, 3, 1, 4),
(663, 49, 3714.2, 1077.12, 3, 1, 3),
(664, 50, 3790, 1099.1, 3, 3, 4),
(665, 55, 4169, 1209.01, 3, 2, 3),
(666, 31, 2349.8, 681.44, 3, 6, 5),
(667, 36, 2728.8, 791.35, 3, 4, 3),
(668, 45, 3411, 989.19, 3, 4, 4),
(669, 56, 4244.8, 1230.99, 3, 5, 5),
(670, 56, 4244.8, 1230.99, 3, 2, 4),
(671, 59, 4472.2, 1296.94, 3, 3, 1),
(672, 67, 5078.6, 1472.79, 3, 4, 5),
(673, 127, 1587.5, 952.5, 4, 3, 5),
(674, 138, 1725, 1035, 4, 3, 1),
(675, 142, 1775, 1065, 4, 2, 5),
(676, 147, 1837.5, 1102.5, 4, 5, 5),
(677, 121, 1512.5, 907.5, 4, 3, 4),
(678, 134, 1675, 1005, 4, 4, 1),
(679, 140, 1750, 1050, 4, 2, 1),
(680, 117, 1462.5, 877.5, 4, 2, 4),
(681, 141, 1762.5, 1057.5, 4, 2, 3),
(682, 110, 1375, 825, 4, 1, 4),
(683, 112, 1400, 840, 4, 6, 5),
(684, 123, 1537.5, 922.5, 4, 3, 3),
(685, 116, 1450, 870, 4, 5, 4),
(686, 123, 1537.5, 922.5, 4, 6, 3),
(687, 134, 1675, 1005, 4, 5, 3),
(688, 129, 1612.5, 967.5, 4, 6, 4),
(689, 136, 1700, 1020, 4, 1, 5),
(690, 116, 1450, 870, 4, 5, 1),
(691, 140, 1750, 1050, 4, 1, 3),
(692, 143, 1787.5, 1072.5, 4, 4, 5),
(693, 111, 1387.5, 832.5, 4, 1, 1),
(694, 117, 1462.5, 877.5, 4, 4, 4),
(695, 131, 1637.5, 982.5, 4, 6, 1),
(696, 140, 1750, 1050, 4, 4, 3),
(697, 116, 986, 640.9, 5, 3, 5),
(698, 127, 1079.5, 701.68, 5, 2, 3),
(699, 110, 935, 607.75, 5, 2, 5),
(700, 115, 977.5, 635.38, 5, 5, 3),
(701, 115, 977.5, 635.38, 5, 1, 3),
(702, 116, 986, 640.9, 5, 6, 1),
(703, 141, 1198.5, 779.03, 5, 3, 1),
(704, 121, 1028.5, 668.53, 5, 4, 4),
(705, 125, 1062.5, 690.63, 5, 4, 1),
(706, 125, 1062.5, 690.63, 5, 1, 1),
(707, 134, 1139, 740.35, 5, 3, 3),
(708, 113, 960.5, 624.33, 5, 4, 5),
(709, 148, 1258, 817.7, 5, 5, 5),
(710, 111, 943.5, 613.28, 5, 1, 4),
(711, 139, 1181.5, 767.98, 5, 6, 5),
(712, 130, 1105, 718.25, 5, 3, 4),
(713, 136, 1156, 751.4, 5, 1, 5),
(714, 139, 1181.5, 767.98, 5, 6, 3),
(715, 129, 1096.5, 712.73, 5, 5, 1),
(716, 120, 1020, 663, 5, 4, 3),
(717, 131, 1113.5, 723.78, 5, 6, 4),
(718, 137, 1164.5, 756.93, 5, 5, 4),
(719, 138, 1173, 762.45, 5, 2, 4),
(720, 149, 1266.5, 823.23, 5, 2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_filiale`
--

DROP TABLE IF EXISTS `tbl_filiale`;
CREATE TABLE IF NOT EXISTS `tbl_filiale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filialenname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filialenname` (`filialenname`),
  UNIQUE KEY `filialenname_2` (`filialenname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `tbl_filiale`
--

INSERT INTO `tbl_filiale` (`id`, `filialenname`) VALUES
(1, 'Bremen'),
(2, 'Dortmund'),
(3, 'Essen'),
(4, 'Frankfurt'),
(6, 'Hamburg'),
(5, 'Köln');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_import`
--

DROP TABLE IF EXISTS `tbl_import`;
CREATE TABLE IF NOT EXISTS `tbl_import` (
  `Filialen` varchar(9) DEFAULT NULL,
  `Quartal` varchar(10) DEFAULT NULL,
  `Artikel` varchar(12) DEFAULT NULL,
  `Anzahl` int(3) DEFAULT NULL,
  `Umsatz` decimal(6,2) DEFAULT NULL,
  `Gewinn` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_import`
--

INSERT INTO `tbl_import` (`Filialen`, `Quartal`, `Artikel`, `Anzahl`, `Umsatz`, `Gewinn`) VALUES
('Dortmund', 'I.Qua', 'Kinderschuhe', 36, '2728.80', '791.35'),
('Hamburg', 'I.Qua', 'Kinderschuhe', 39, '2956.20', '857.30'),
('Essen', 'IV.Qua', 'Pflegemittel', 127, '1587.50', '952.50'),
('Dortmund', 'IV.Qua', 'Herrenschuhe', 32, '3155.20', '1009.66'),
('Essen', 'I.Qua', 'Pflegemittel', 138, '1725.00', '1035.00'),
('Dortmund', 'IV.Qua', 'Pflegemittel', 142, '1775.00', '1065.00'),
('Köln', 'IV.Qua', 'Pflegemittel', 147, '1837.50', '1102.50'),
('Bremen', 'I.Qua', 'Kinderschuhe', 52, '3941.60', '1143.06'),
('Hamburg', 'III.Qua', 'Herrenschuhe', 37, '3648.20', '1167.42'),
('Frankfurt', 'IV.Qua', 'Damenschuhe', 32, '3600.00', '1260.00'),
('Essen', 'II.Qua', 'Damenschuhe', 36, '4050.00', '1417.50'),
('Dortmund', 'II.Qua', 'Herrenschuhe', 45, '4437.00', '1419.84'),
('Hamburg', 'I.Qua', 'Herrenschuhe', 53, '5225.80', '1672.26'),
('Köln', 'II.Qua', 'Herrenschuhe', 57, '5620.20', '1798.46'),
('Bremen', 'IV.Qua', 'Herrenschuhe', 67, '6606.20', '2113.98'),
('Hamburg', 'III.Qua', 'Damenschuhe', 62, '6975.00', '2441.25'),
('Frankfurt', 'III.Qua', 'Damenschuhe', 68, '7650.00', '2677.50'),
('Essen', 'IV.Qua', 'Zubehör', 116, '986.00', '640.90'),
('Dortmund', 'II.Qua', 'Zubehör', 127, '1079.50', '701.68'),
('Essen', 'III.Qua', 'Pflegemittel', 121, '1512.50', '907.50'),
('Frankfurt', 'I.Qua', 'Pflegemittel', 134, '1675.00', '1005.00'),
('Dortmund', 'I.Qua', 'Pflegemittel', 140, '1750.00', '1050.00'),
('Hamburg', 'IV.Qua', 'Damenschuhe', 30, '3375.00', '1181.25'),
('Essen', 'IV.Qua', 'Herrenschuhe', 66, '6507.60', '2082.43'),
('Köln', 'II.Qua', 'Damenschuhe', 58, '6525.00', '2283.75'),
('Bremen', 'I.Qua', 'Damenschuhe', 58, '6525.00', '2283.75'),
('Dortmund', 'IV.Qua', 'Zubehör', 110, '935.00', '607.75'),
('Köln', 'II.Qua', 'Zubehör', 115, '977.50', '635.38'),
('Bremen', 'II.Qua', 'Zubehör', 115, '977.50', '635.38'),
('Hamburg', 'I.Qua', 'Zubehör', 116, '986.00', '640.90'),
('Essen', 'I.Qua', 'Zubehör', 141, '1198.50', '779.03'),
('Dortmund', 'III.Qua', 'Pflegemittel', 117, '1462.50', '877.50'),
('Köln', 'II.Qua', 'Kinderschuhe', 41, '3107.80', '901.26'),
('Frankfurt', 'I.Qua', 'Kinderschuhe', 41, '3107.80', '901.26'),
('Dortmund', 'II.Qua', 'Pflegemittel', 141, '1762.50', '1057.50'),
('Bremen', 'I.Qua', 'Herrenschuhe', 37, '3648.20', '1167.42'),
('Essen', 'II.Qua', 'Herrenschuhe', 47, '4634.20', '1482.94'),
('Dortmund', 'III.Qua', 'Herrenschuhe', 52, '5127.20', '1640.70'),
('Hamburg', 'IV.Qua', 'Herrenschuhe', 70, '6902.00', '2208.64'),
('Köln', 'I.Qua', 'Damenschuhe', 57, '6412.50', '2244.38'),
('Dortmund', 'I.Qua', 'Damenschuhe', 61, '6862.50', '2401.88'),
('Dortmund', 'IV.Qua', 'Damenschuhe', 64, '7200.00', '2520.00'),
('Bremen', 'IV.Qua', 'Damenschuhe', 68, '7650.00', '2677.50'),
('Frankfurt', 'III.Qua', 'Zubehör', 121, '1028.50', '668.53'),
('Frankfurt', 'I.Qua', 'Zubehör', 125, '1062.50', '690.63'),
('Bremen', 'I.Qua', 'Zubehör', 125, '1062.50', '690.63'),
('Essen', 'II.Qua', 'Zubehör', 134, '1139.00', '740.35'),
('Bremen', 'III.Qua', 'Pflegemittel', 110, '1375.00', '825.00'),
('Hamburg', 'IV.Qua', 'Pflegemittel', 112, '1400.00', '840.00'),
('Essen', 'II.Qua', 'Pflegemittel', 123, '1537.50', '922.50'),
('Hamburg', 'II.Qua', 'Herrenschuhe', 30, '2958.00', '946.56'),
('Essen', 'II.Qua', 'Kinderschuhe', 45, '3411.00', '989.19'),
('Köln', 'I.Qua', 'Kinderschuhe', 49, '3714.20', '1077.12'),
('Hamburg', 'III.Qua', 'Kinderschuhe', 61, '4623.80', '1340.90'),
('Dortmund', 'IV.Qua', 'Kinderschuhe', 67, '5078.60', '1472.79'),
('Frankfurt', 'III.Qua', 'Herrenschuhe', 52, '5127.20', '1640.70'),
('Frankfurt', 'IV.Qua', 'Zubehör', 113, '960.50', '624.33'),
('Köln', 'IV.Qua', 'Zubehör', 148, '1258.00', '817.70'),
('Köln', 'III.Qua', 'Pflegemittel', 116, '1450.00', '870.00'),
('Hamburg', 'II.Qua', 'Kinderschuhe', 40, '3032.00', '879.28'),
('Hamburg', 'II.Qua', 'Pflegemittel', 123, '1537.50', '922.50'),
('Köln', 'II.Qua', 'Pflegemittel', 134, '1675.00', '1005.00'),
('Köln', 'III.Qua', 'Kinderschuhe', 48, '3638.40', '1055.14'),
('Bremen', 'IV.Qua', 'Kinderschuhe', 57, '4320.60', '1252.97'),
('Frankfurt', 'IV.Qua', 'Herrenschuhe', 53, '5225.80', '1672.26'),
('Bremen', 'II.Qua', 'Herrenschuhe', 53, '5225.80', '1672.26'),
('Hamburg', 'I.Qua', 'Damenschuhe', 43, '4837.50', '1693.13'),
('Essen', 'IV.Qua', 'Damenschuhe', 48, '5400.00', '1890.00'),
('Dortmund', 'I.Qua', 'Herrenschuhe', 67, '6606.20', '2113.98'),
('Dortmund', 'III.Qua', 'Damenschuhe', 57, '6412.50', '2244.38'),
('Bremen', 'III.Qua', 'Zubehör', 111, '943.50', '613.28'),
('Hamburg', 'IV.Qua', 'Zubehör', 139, '1181.50', '767.98'),
('Hamburg', 'III.Qua', 'Pflegemittel', 129, '1612.50', '967.50'),
('Bremen', 'IV.Qua', 'Pflegemittel', 136, '1700.00', '1020.00'),
('Essen', 'IV.Qua', 'Kinderschuhe', 64, '4851.20', '1406.85'),
('Köln', 'III.Qua', 'Damenschuhe', 51, '5737.50', '2008.13'),
('Essen', 'III.Qua', 'Zubehör', 130, '1105.00', '718.25'),
('Bremen', 'IV.Qua', 'Zubehör', 136, '1156.00', '751.40'),
('Hamburg', 'II.Qua', 'Zubehör', 139, '1181.50', '767.98'),
('Köln', 'I.Qua', 'Pflegemittel', 116, '1450.00', '870.00'),
('Bremen', 'III.Qua', 'Kinderschuhe', 45, '3411.00', '989.19'),
('Köln', 'I.Qua', 'Herrenschuhe', 32, '3155.20', '1009.66'),
('Bremen', 'II.Qua', 'Pflegemittel', 140, '1750.00', '1050.00'),
('Frankfurt', 'IV.Qua', 'Pflegemittel', 143, '1787.50', '1072.50'),
('Bremen', 'II.Qua', 'Kinderschuhe', 49, '3714.20', '1077.12'),
('Essen', 'III.Qua', 'Kinderschuhe', 50, '3790.00', '1099.10'),
('Dortmund', 'II.Qua', 'Kinderschuhe', 55, '4169.00', '1209.01'),
('Köln', 'III.Qua', 'Herrenschuhe', 53, '5225.80', '1672.26'),
('Hamburg', 'IV.Qua', 'Kinderschuhe', 31, '2349.80', '681.44'),
('Köln', 'I.Qua', 'Zubehör', 129, '1096.50', '712.73'),
('Frankfurt', 'II.Qua', 'Herrenschuhe', 33, '3253.80', '1041.22'),
('Essen', 'I.Qua', 'Damenschuhe', 32, '3600.00', '1260.00'),
('Hamburg', 'II.Qua', 'Damenschuhe', 33, '3712.50', '1299.38'),
('Köln', 'IV.Qua', 'Herrenschuhe', 43, '4239.80', '1356.74'),
('Essen', 'III.Qua', 'Damenschuhe', 42, '4725.00', '1653.75'),
('Köln', 'IV.Qua', 'Damenschuhe', 47, '5287.50', '1850.63'),
('Bremen', 'III.Qua', 'Damenschuhe', 47, '5287.50', '1850.63'),
('Essen', 'III.Qua', 'Herrenschuhe', 67, '6606.20', '2113.98'),
('Frankfurt', 'II.Qua', 'Damenschuhe', 63, '7087.50', '2480.63'),
('Frankfurt', 'II.Qua', 'Zubehör', 120, '1020.00', '663.00'),
('Hamburg', 'III.Qua', 'Zubehör', 131, '1113.50', '723.78'),
('Köln', 'III.Qua', 'Zubehör', 137, '1164.50', '756.93'),
('Dortmund', 'III.Qua', 'Zubehör', 138, '1173.00', '762.45'),
('Frankfurt', 'II.Qua', 'Kinderschuhe', 36, '2728.80', '791.35'),
('Dortmund', 'I.Qua', 'Zubehör', 149, '1266.50', '823.23'),
('Bremen', 'I.Qua', 'Pflegemittel', 111, '1387.50', '832.50'),
('Frankfurt', 'III.Qua', 'Pflegemittel', 117, '1462.50', '877.50'),
('Hamburg', 'I.Qua', 'Pflegemittel', 131, '1637.50', '982.50'),
('Frankfurt', 'III.Qua', 'Kinderschuhe', 45, '3411.00', '989.19'),
('Frankfurt', 'II.Qua', 'Pflegemittel', 140, '1750.00', '1050.00'),
('Frankfurt', 'I.Qua', 'Herrenschuhe', 34, '3352.40', '1072.77'),
('Köln', 'IV.Qua', 'Kinderschuhe', 56, '4244.80', '1230.99'),
('Dortmund', 'III.Qua', 'Kinderschuhe', 56, '4244.80', '1230.99'),
('Essen', 'I.Qua', 'Kinderschuhe', 59, '4472.20', '1296.94'),
('Frankfurt', 'IV.Qua', 'Kinderschuhe', 67, '5078.60', '1472.79'),
('Bremen', 'III.Qua', 'Herrenschuhe', 48, '4732.80', '1514.50'),
('Essen', 'I.Qua', 'Herrenschuhe', 67, '6606.20', '2113.98'),
('Frankfurt', 'I.Qua', 'Damenschuhe', 60, '6750.00', '2362.50'),
('Dortmund', 'II.Qua', 'Damenschuhe', 60, '6750.00', '2362.50'),
('Bremen', 'II.Qua', 'Damenschuhe', 61, '6862.50', '2401.88');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_quartal`
--

DROP TABLE IF EXISTS `tbl_quartal`;
CREATE TABLE IF NOT EXISTS `tbl_quartal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quartal` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `quartal` (`quartal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `tbl_quartal`
--

INSERT INTO `tbl_quartal` (`id`, `quartal`) VALUES
(1, 'I.Qua'),
(3, 'II.Qua'),
(4, 'III.Qua'),
(5, 'IV.Qua');

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `vws_cube`
--
DROP VIEW IF EXISTS `vws_cube`;
CREATE TABLE IF NOT EXISTS `vws_cube` (
`Anzahl` int(11)
,`Umsatz` double
,`Gewinn` double
,`Artikel` varchar(50)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `vws_cube1`
--
DROP VIEW IF EXISTS `vws_cube1`;
CREATE TABLE IF NOT EXISTS `vws_cube1` (
`Anzahl` int(11)
,`Umsatz` double
,`Gewinn` double
,`Artikel` varchar(50)
,`filialenname` varchar(50)
,`quartal` varchar(20)
);
-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `vws_test`
--
DROP VIEW IF EXISTS `vws_test`;
CREATE TABLE IF NOT EXISTS `vws_test` (
`Anzahl` int(11)
,`Umsatz` double
,`Gewinn` double
,`Artikel` varchar(50)
,`filialenname` varchar(50)
,`quartal` varchar(20)
);
-- --------------------------------------------------------

--
-- Struktur des Views `vws_cube`
--
DROP TABLE IF EXISTS `vws_cube`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vws_cube` AS select `tbl_fact`.`Anzahl` AS `Anzahl`,`tbl_fact`.`Umsatz` AS `Umsatz`,`tbl_fact`.`Gewinn` AS `Gewinn`,`a`.`Artikel` AS `Artikel` from (`tbl_fact` join `tbl_artikel` `a` on((`a`.`id` = `tbl_fact`.`fk_artikel`)));

-- --------------------------------------------------------

--
-- Struktur des Views `vws_cube1`
--
DROP TABLE IF EXISTS `vws_cube1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vws_cube1` AS select `tbl_fact`.`Anzahl` AS `Anzahl`,`tbl_fact`.`Umsatz` AS `Umsatz`,`tbl_fact`.`Gewinn` AS `Gewinn`,`a`.`Artikel` AS `Artikel`,`f`.`filialenname` AS `filialenname`,`q`.`quartal` AS `quartal` from (((`tbl_fact` join `tbl_artikel` `a` on((`a`.`id` = `tbl_fact`.`fk_artikel`))) join `tbl_filiale` `f` on((`f`.`id` = `tbl_fact`.`fk_filiale`))) join `tbl_quartal` `q` on((`q`.`id` = `tbl_fact`.`fk_quartal`)));

-- --------------------------------------------------------

--
-- Struktur des Views `vws_test`
--
DROP TABLE IF EXISTS `vws_test`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vws_test` AS select `tbl_fact`.`Anzahl` AS `Anzahl`,`tbl_fact`.`Umsatz` AS `Umsatz`,`tbl_fact`.`Gewinn` AS `Gewinn`,`a`.`Artikel` AS `Artikel`,`f`.`filialenname` AS `filialenname`,`q`.`quartal` AS `quartal` from (((`tbl_fact` join `tbl_artikel` `a` on((`a`.`id` = `tbl_fact`.`fk_artikel`))) join `tbl_filiale` `f` on((`f`.`id` = `tbl_fact`.`fk_filiale`))) join `tbl_quartal` `q` on((`q`.`id` = `tbl_fact`.`fk_quartal`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
