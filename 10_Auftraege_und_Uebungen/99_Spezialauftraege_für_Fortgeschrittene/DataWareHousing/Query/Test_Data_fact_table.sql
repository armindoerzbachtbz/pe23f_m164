select 
       tbl_artikel.id as fk_artikel,tbl_artikel.Artikel,
	   tbl_filiale.id as fk_filiale, tbl_filiale.filialenname,
	   tbl_quartal.id as fk_quartal,tbl_quartal.Quartal,
	   tbl_import.Anzahl,
	   tbl_import.Umsatz,
	    tbl_import.Gewinn
from   tbl_filiale, tbl_import, tbl_artikel, tbl_quartal

where     tbl_import.Filialen=tbl_filiale.filialenname 
      and tbl_import.Artikel=tbl_artikel.Artikel 
	  and tbl_import.Quartal=tbl_quartal.quartal
	  and tbl_filiale.filialenname='Essen'