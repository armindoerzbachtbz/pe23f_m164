
View Data Mart 
CREATE VIEW vws_cube1 AS  
select   
  tbl_fact.Anzahl AS Anzahl,  
  tbl_fact.Umsatz AS Umsatz,  
  tbl_fact.Gewinn AS Gewinn,  
  a.Artikel AS Artikel,  
  f.filialenname AS filialenname,    
  q.quartal AS quartal
  from    
    tbl_fact
       join tbl_artikel a ON a.id = tbl_fact.fk_artikel
       join tbl_filiale f ON f.id = tbl_fact.fk_filiale
       join tbl_quartal q ON q.id = tbl_fact.fk_quartal

==============================================================
Variant 2
CREATE VIEW vws_cube_V2 AS  
Select 
  tbl_filiale.filialenname, 
  tbl_quartal.quartal, 
  tbl_artikel.artikel, 
  tbl_fact.anzahl, 
  tbl_fact.umsatz, 
  tbl_fact.gewinn 
  from 
    tbl_artikel 
    join ( tbl_filiale join (tbl_quartal 
      join tbl_fact on tbl_quartal.id = tbl_fact.fk_quartal) on    
        tbl_filiale.id = tbl_fact.fk_filiale) on tbl_artikel.id = 
        tbl_fact.fk_artikel 

