-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 16. Dez 2014 um 14:48
-- Server Version: 5.6.16
-- PHP-Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `pivot`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_artikel`
--

CREATE TABLE IF NOT EXISTS `tbl_artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Artikel` varchar(50) NOT NULL,
  `Beschreibung` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Daten für Tabelle `tbl_artikel`
--

INSERT INTO `tbl_artikel` (`id`, `Artikel`, `Beschreibung`) VALUES
(1, 'Herrenschuhe', ''),
(2, 'Damenschuhe', ''),
(3, 'Kinderschuhe', ''),
(4, 'Pflegemittel', ''),
(5, 'Zubehör', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_fact`
--

CREATE TABLE IF NOT EXISTS `tbl_fact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Anzahl` int(11) NOT NULL,
  `Umsatz` double NOT NULL,
  `Gewinn` double NOT NULL,
  `fk_artikel` int(11) NOT NULL,
  `fk_filiale` int(11) NOT NULL,
  `fk_quartal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_artikel` (`fk_artikel`),
  KEY `fk_filiale` (`fk_filiale`),
  KEY `fk_quartal` (`fk_quartal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Daten für Tabelle `tbl_fact`
--

INSERT INTO `tbl_fact` (`id`, `Anzahl`, `Umsatz`, `Gewinn`, `fk_artikel`, `fk_filiale`, `fk_quartal`) VALUES
(1, 52, 3941.6, 1143.06, 3, 1, 1),
(2, 58, 6525, 2283.75, 2, 1, 1),
(3, 37, 3648.2, 1167.42, 1, 1, 1),
(4, 125, 1062.5, 690.63, 5, 1, 1),
(5, 111, 1387.5, 832.5, 4, 1, 1),
(6, 115, 977.5, 635.38, 5, 1, 3),
(7, 53, 5225.8, 1672.26, 1, 1, 3),
(9, 140, 1750, 1050, 4, 1, 3),
(10, 49, 3714.2, 1077.12, 3, 1, 3),
(11, 61, 6852.5, 2401.88, 2, 1, 3),
(12, 110, 3941.6, 1143.06, 4, 1, 4),
(13, 111, 6525, 2283.75, 5, 1, 4),
(14, 45, 3648.2, 1167.42, 3, 1, 4),
(15, 47, 1062.5, 690.63, 2, 1, 4),
(16, 48, 1387.5, 832.5, 1, 1, 4),
(17, 67, 6606.2, 2113.98, 1, 1, 5),
(18, 68, 7650, 2677.5, 2, 1, 5),
(19, 57, 4320.6, 1252.97, 3, 1, 5),
(20, 136, 1700, 1020, 2, 1, 5),
(21, 136, 1156, 751.4, 1, 1, 5),
(22, 36, 2728.8, 791.35, 3, 2, 1),
(23, 140, 1750, 1050, 4, 2, 1),
(24, 61, 6862.5, 2401.88, 2, 2, 1),
(25, 67, 6606.2, 2113.98, 1, 2, 1),
(26, 149, 1266.5, 823.23, 5, 2, 1),
(27, 45, 4437, 1419.84, 1, 2, 3),
(28, 127, 1079.5, 701.68, 5, 2, 3),
(29, 141, 1762.5, 1057.5, 4, 2, 3),
(30, 55, 4169, 1209.01, 2, 2, 3),
(31, 60, 6750, 2362.5, 2, 2, 3),
(32, 117, 1462.5, 877.5, 4, 2, 4),
(33, 52, 5127.2, 1640.7, 1, 2, 4),
(34, 57, 6412.5, 2244.38, 2, 2, 4),
(35, 138, 1173, 762.45, 2, 2, 4),
(36, 56, 4244.8, 1230.99, 3, 2, 4),
(37, 32, 31555.2, 1009.66, 1, 2, 5),
(38, 142, 1775, 1065, 4, 2, 5),
(39, 110, 935, 607.75, 5, 2, 5),
(40, 64, 7200, 2520, 2, 2, 5),
(41, 67, 5078.6, 1472.79, 3, 2, 5),
(42, 138, 1725, 1035, 4, 3, 1),
(43, 141, 1198.5, 779.03, 5, 3, 1),
(44, 32, 3600, 1260, 2, 3, 1),
(45, 59, 4472.2, 1296.94, 3, 3, 1),
(46, 67, 6606.2, 2113.98, 1, 3, 1),
(47, 36, 4050, 1417.5, 2, 3, 3),
(48, 47, 4634.2, 1482.94, 1, 3, 3),
(49, 134, 1139, 740.35, 5, 3, 3),
(50, 123, 1537.5, 922.5, 4, 3, 3),
(51, 45, 3411, 989.19, 3, 3, 3),
(52, 121, 1512.5, 907.5, 4, 3, 4),
(53, 130, 1105, 718.25, 5, 3, 4),
(54, 50, 3790, 1099.1, 3, 3, 4),
(55, 42, 4725, 1653.75, 2, 3, 4),
(56, 67, 6606.2, 2113.98, 1, 3, 4),
(57, 127, 1587.5, 952.5, 4, 3, 5),
(58, 116, 986, 640.9, 5, 3, 5),
(59, 66, 6507.6, 2082.43, 1, 3, 5),
(60, 48, 5400, 1890, 2, 3, 5),
(61, 64, 4851.2, 1406.85, 3, 3, 5),
(62, 134, 1675, 1005, 4, 4, 1),
(63, 41, 3107.8, 901.26, 3, 4, 1),
(64, 125, 1062.5, 690.63, 5, 4, 1),
(65, 34, 3352.4, 1072.77, 1, 4, 1),
(66, 60, 6750, 2362.5, 2, 4, 1),
(67, 33, 3253.8, 1041.22, 1, 4, 3),
(68, 63, 7087.5, 2480, 2, 4, 3),
(69, 120, 1020, 663, 5, 4, 3),
(70, 36, 2728.8, 791, 3, 4, 3),
(71, 140, 1750, 1050, 4, 4, 3),
(72, 68, 7650, 2677.5, 2, 4, 4),
(73, 121, 1028.5, 668, 5, 4, 4),
(74, 52, 5127.2, 1640, 1, 4, 4),
(75, 117, 1462.5, 877.5, 4, 4, 4),
(76, 45, 3411, 989, 3, 4, 4),
(77, 32, 3600, 1260, 2, 4, 4),
(78, 113, 960.5, 624, 5, 4, 4),
(79, 53, 5225.2, 1672, 1, 4, 4),
(80, 143, 1787.5, 1072.5, 4, 4, 4),
(81, 67, 5078.2, 1472, 3, 4, 4),
(82, 39, 2956.2, 857, 3, 6, 1),
(83, 53, 5225.8, 1672, 1, 6, 1),
(84, 116, 986, 640, 5, 6, 1),
(85, 43, 4837.5, 1693, 3, 6, 1),
(86, 131, 1637.5, 982, 1, 6, 4),
(87, 30, 2958, 946.56, 1, 6, 3),
(88, 40, 3032, 879.28, 3, 6, 3),
(89, 123, 1537.5, 922.5, 2, 6, 3),
(90, 139, 1181.5, 767, 5, 6, 3),
(91, 33, 3712.5, 1299, 2, 6, 3),
(92, 37, 3648, 1167.56, 1, 6, 4),
(93, 62, 6975, 2441.28, 2, 6, 4),
(94, 61, 4623.5, 1340.5, 3, 6, 4),
(95, 129, 1612.5, 967, 5, 6, 4),
(96, 131, 1113.5, 723, 4, 6, 4),
(97, 30, 3375, 1181.56, 1, 6, 5),
(98, 70, 6902, 6902, 2, 6, 5),
(99, 112, 1400.5, 840, 3, 6, 5),
(100, 139, 1181.5, 767, 5, 6, 5),
(101, 31, 2349.5, 681, 4, 6, 5),
(102, 57, 6412, 2244, 2, 5, 1),
(103, 49, 3714, 1077, 3, 5, 1),
(104, 116, 1450.5, 870, 4, 5, 1),
(105, 32, 3155.5, 1009, 1, 5, 1),
(106, 129, 1096.5, 712, 5, 5, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_filiale`
--

CREATE TABLE IF NOT EXISTS `tbl_filiale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filialenname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filialenname` (`filialenname`),
  UNIQUE KEY `filialenname_2` (`filialenname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Daten für Tabelle `tbl_filiale`
--

INSERT INTO `tbl_filiale` (`id`, `filialenname`) VALUES
(1, 'Bremen'),
(2, 'Dortmund'),
(3, 'Essen'),
(4, 'Frankfurt'),
(6, 'Hamburg'),
(5, 'Köln');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_quartal`
--

CREATE TABLE IF NOT EXISTS `tbl_quartal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quartal` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `quartal` (`quartal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Daten für Tabelle `tbl_quartal`
--

INSERT INTO `tbl_quartal` (`id`, `quartal`) VALUES
(1, 'I. Qua.'),
(3, 'II. Qua.'),
(4, 'III. Qua.'),
(5, 'IV. Qua.');

-- --------------------------------------------------------


--
-- Struktur des Views `vws_cube`
--
DROP TABLE IF EXISTS `vws_cube`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vws_cube` AS select `tbl_fact`.`Anzahl` AS `Anzahl`,`tbl_fact`.`Umsatz` AS `Umsatz`,`tbl_fact`.`Gewinn` AS `Gewinn`,`a`.`Artikel` AS `Artikel` from (`tbl_fact` join `tbl_artikel` `a` on((`a`.`id` = `tbl_fact`.`fk_artikel`)));

-- --------------------------------------------------------

--
-- Struktur des Views `vws_cube1`
--
DROP TABLE IF EXISTS `vws_cube1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vws_cube1` AS select `tbl_fact`.`Anzahl` AS `Anzahl`,`tbl_fact`.`Umsatz` AS `Umsatz`,`tbl_fact`.`Gewinn` AS `Gewinn`,`a`.`Artikel` AS `Artikel`,`f`.`filialenname` AS `filialenname`,`q`.`quartal` AS `quartal` from (((`tbl_fact` join `tbl_artikel` `a` on((`a`.`id` = `tbl_fact`.`fk_artikel`))) join `tbl_filiale` `f` on((`f`.`id` = `tbl_fact`.`fk_filiale`))) join `tbl_quartal` `q` on((`q`.`id` = `tbl_fact`.`fk_quartal`)));

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tbl_fact`
--
ALTER TABLE `tbl_fact`
  ADD CONSTRAINT `tbl_fact_ibfk_3` FOREIGN KEY (`fk_quartal`) REFERENCES `tbl_quartal` (`id`),
  ADD CONSTRAINT `tbl_fact_ibfk_1` FOREIGN KEY (`fk_artikel`) REFERENCES `tbl_artikel` (`id`),
  ADD CONSTRAINT `tbl_fact_ibfk_2` FOREIGN KEY (`fk_filiale`) REFERENCES `tbl_filiale` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vws_cube1` AS 
select `tbl_fact`.`Anzahl` AS `Anzahl`,`tbl_fact`.`Umsatz` 
                           AS `Umsatz`,`tbl_fact`.`Gewinn` 
						   AS `Gewinn`,`a`.`Artikel` 
						   AS `Artikel`,`f`.`filialenname` 
						   AS `filialenname`,`q`.`quartal` 
						   AS `quartal` 
						   from (((`tbl_fact` join `tbl_artikel` `a` on((`a`.`id` = `tbl_fact`.`fk_artikel`))) 
						                      join `tbl_filiale` `f` on((`f`.`id` = `tbl_fact`.`fk_filiale`))) 
											  join `tbl_quartal` `q` on((`q`.`id` = `tbl_fact`.`fk_quartal`)));


