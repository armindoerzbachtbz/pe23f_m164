# Probeprüfung LB2

Zeit: 60 min, Maximale Punktzahl 32, Note 6 bei 28 Punkten, Note 5 bei 22 Note, Note 4 bei 17 Punkten

Sie bekommen eine Datenbank `classic_store` als dump geliefert. In dieser sollen sie dann einige Aufgaben lösen. 

Sie muessen alle Kommandos die sie ausführen und deren Output in einem File classic_store_journal.txt speichern.

Hier das ERM:
![ERM](./classic_store.png)

## Die Aufgaben

1. Importieren Sie die Datenbank [classic_store](./classic_store.sql) (2 Punkte)

2. Aktualisieren Sie den Produktpreis für das Produkt mit der ID 101 in der "products" Tabelle auf 55.00. (1 Punkt)

3. Setzen Sie in der Tabelle users die Email-Adressen wie folgt: "user{user_id}@example.com".  
{user_id} entspricht dem Primary Key `user_id` der Tabelle `users`. (3 Punkte)

4. Schreiben Sie eine SQL-Abfrage, um den Namen und die E-Mail-Adresse aller Benutzer anzuzeigen, die mindestens eine Bestellung aufgegeben haben, zusammen mit den Produkten, die sie bestellt haben.(3 Punkte)

5. Formulieren Sie eine SQL-Abfrage, um die Anzahl der Bestellungen pro Benutzer anzuzeigen, wobei die Benutzerinformationen sowie die Anzahl der Bestellungen für jeden Benutzer angezeigt werden. (3 Punkte)

6. Schreiben Sie eine SQL-Abfrage, um die Anzahl der Produkte pro Kategorie auszugeben, zusammen mit dem Namen der Kategorie. Sortieren Sie das Ergebnis nach der Anzahl der Produkte in absteigender Reihenfolge. (3 Punkte)
   
7.  Schreiben sie eine SQL-Abfrage, um alle Benutzer anzuzeigen, die mindestens zwei Bestellungen getätigt haben. Geben Sie die Email-Adresse, die Anzahl der Bestellungen und die Anzahl aller Items aus. (3 Punkte)
   
8.  Schreiben Sie eine SQL-Abfrage, um alle Benutzer anzuzeigen, die mindestens zwei Bestellungen getätigt haben und deren Gesamtbetrag aller Bestellungen mehr als 300.00 beträgt. Geben Sie die Email-Adresse, die Anzahl der Bestellungen und den Gesamtbetrag aus. (5 Punkte)

9.  Der Kunde mit der user_id=1 möchte, dass seine Daten gelöscht werden. Löschen sie diese. (2 Punkte)

10. Der Boss moechte eine Auswertung für alle Produkte der `Category E` und `Category A`. Er möchte in einer Tabelle die durchschnittlichen Ratings, Stückzahl für jedes Produkt die Verkauft worden ist und den Umsatz, d.h. wieviele Franken pro Produkt eingenommen worden sind. Berechnen sie das mit einem SQL-Statement. (6 Punkte)
    
11. Machen sie einen export der Datenbank mit mysqldump und speichern sie diesen im file classic_store_after.sql (1 Punkt)