
-- NO ACTION / RESTRICT / CASCADE / SET NULL
DELETE FROM foreignkey_tests.orte; -- geht nicht wegen constraint

-- 
DELETE FROM foreignkey_tests.orte WHERE Name = 'Emmendingen' ;   
UPDATE orte SET postleitzahl = 99999 WHERE postleitzahl = 10000;
UPDATE kunden SET postleitzahl_FK = 79312 WHERE postleitzahl_FK = 10000;
UPDATE kunden SET postleitzahl_FK = 99999 WHERE postleitzahl_FK = 79092;
-- 
SELECT * FROM foreignkey_tests.orte;
SELECT * FROM foreignkey_tests.kunden;