#  03 Create table

Zeit: 15 Min.<br>
Form: 2er Team

Auftrag: Erzeugt eine Tabelle `Person` mit folgenden Attributen nur mit Hilfe von einem `create table` statement. Passt ein Statement welches ihr im  Auftrag [Forward Engineering](03_forward_engineering.md) generiert habt, richtig an:

| Attribut     | Datentyp    |
|--------------|-------------|
| PersonID     | INT         |
| Name         | VARCHAR(50) |
| Vorname      | VARCHAR(50) |
| Geburtsdatum | DATETIME    |
| Gewicht      | INT(3)      |
| Einkommen    | FLOAT(10,2) |

 
