# Auftrag: Storage in einer Datenbank erklären

[TOC]

Zeit: 45 Min.<br>
Form: Individuell oder 2er Team

## Recherche und Zusammenfassung

Recherchieren Sie folgende Begriffe im Internet und erstellen Sie eine Zusammenfassung (höchstens 1 A4 pro Thema).
1. Tablespace, Tablespace Architecture
2. Partition
3. Was macht eine *storage engine* in einer Datenbank?

## Kurze Erklärung im Plenum
Erklären Sie der Klasse ihr erworbenes Wissen zu den obigen Themen.

Pro Thema max. 2-3 Min.

---
