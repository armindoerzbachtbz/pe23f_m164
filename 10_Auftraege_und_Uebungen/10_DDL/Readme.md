# Aufträge zu DDL-Statements
[Use Forward Engineering](./03_forward_engineering.md)<br>
[Create Database/Schema](./03_create_schema.md)<br>
[Create Table](./03_create_table.md)<br>
[Drop table/schema/database](./03_drop_statement.md)<br>
[Alter table](./05_alter_table.md)
[Use Synchronize Model](./03_synchronize_modell.md)<br>
