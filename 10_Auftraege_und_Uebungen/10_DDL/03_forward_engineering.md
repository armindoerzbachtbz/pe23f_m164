# Forward Engineering

Zeit: 30 Min.
Form: 2er Team

Wenn man schon ein Modell (ERD) erzeugt hat, gibt es in Mysql-Workbench unter Database **-> Forward Engineer** die Möglichkeit direkt eine Datenbank aus dem Model erzeugen zu lassen. In diesem Prozess werden alle SQL-Statements angezeigt und ggf. ausgeführt,  welche es dazu braucht.

**Aufgabe**:
Erzeugt ein Model in welchem ihr einige Relationen und Tabellen habt, mit verschiedenen Attributen.
Dann sollt ihr mit Forward-Engineering die SQL-Statements erzeugen lassen und diese als Vorlage sichern.

**Forward Engineering** wählen:

![](./resourcen/03_forward_engineering_1.png)

Als nächstes die **IP-Adresse** des Datenbank-Servers eingeben (z.b. TBZ-Cloudserver, localhost (127.0.0.1)  oder AWS Instanz). Benutzernamen und Passwort haben Sie entweder selbst gesetzt oder wird Ihnen von der Lehrperson mitgeteilt.

![](./resourcen/03_forward_engineering_2.png)

Dann wählen, welche **Statements** man generieren lassen will: *(Anzeige kann je nach Version variieren!)*

![](./resourcen/03_forward_engineering_3.png)

Ggf. **filtern**, was man alles nicht erzeugen lassen will:

![](./resourcen/03_forward_engineering_4.png)

Ein SQL-Skript wird generiert, aus welchem man die Statements "**erlernen**" kann:

![](./resourcen/03_forward_engineering_5.png)


**Save to file ...** Diese Statements als "Vorlage" irgendwo wegsichern, am besten in das Lernjournal.

Mit **Next** wird das SQL-Script dem DB-Server übermittelt. Verbinden Sie sich nun mit dem DB-Server und lassen Sie sich die neu erstellte Datenstruktur anzeigen:

![](./resourcen/03_forward_engineering_6.png)

