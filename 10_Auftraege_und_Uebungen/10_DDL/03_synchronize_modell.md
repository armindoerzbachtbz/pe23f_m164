# 03 Synchronize Modell

Zeit: 30 Min.
Form: 2er Team

Mit Synchronize Modell kann man das Modell mit der erzeugten DB vergleichen und synchronisieren. Dabei werden die Definitionen von Tabellen und Relationen im Modell mit einer laufenden Datenbank verglichen und allenfalls synchronisiert.  

Achtung der Datenbank- oder Schema-Name hängt vom Physical Schema Name ab, im Beispiel hier `mydb`
![](/03_sql_auftraege/resourcen/03_sync_modell_1.png)

Im Fall man dies ändern möchte, kann man mit rechter Maustaste auf dem Namen klicken und dann `Edit Schema` wählen:
![](/03_sql_auftraege/resourcen/03_sync_modell_2.png)

Danach kann man Unter `Database -> Synchronize Modell` die Datenbank erzeugt, beziehungsweise angepasst werden.

Damit ihr ein bisschen ein Gefühl für die Möglichkeiten kriegt macht ihr folgenden Auftrag:

1. Erzeugt ein Modell mit dem DB-Schema Namen `<ihrname>_testdb` in welchem ihr mindestens 2 Tabellen mit einer Relation habt.
2. Synchronisiert das Modell mit dem Schema.
3. Erzeugt im Modell eine zusätzliche Relation zwischen 2 bestehenden Tabellen.
4. Erzeugt ein zusätzliches Attribut in einer bestehenden Tabelle.
5. Synchronisiert das Modell und schaut euch den generierten SQL-Code gut an. Welche statements ändern Tabellen?
6. Versucht allerlei Modifikationen am Modell zu machen und schaut euch die resultierenden SQL-Statments dazu an. 