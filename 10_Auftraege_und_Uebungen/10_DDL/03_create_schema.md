# 03 Create Schema or Create Database

Zeit: 15 Min.<br>
Form: 2er Team

In Mysql kann man ein Schema/Database mit einen SQL-Statment als root user erzeugen.

Zuerst muss man als root-User einloggen, und dann das `create schema` statement benutzen welches im Auftrag [Forward Engineering](03_forward_engineering.md) erzeugt wurde benutzen um die Datenbank zu erzeugen.

Auftrag:

Erzeugt eine Datenbank mit dem Namen `<ihrvorname>_test` in der TBZ cloud oder lokal.
