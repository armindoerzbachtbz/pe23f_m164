# ---- Recap m162 (ADO) ---

**Zeitvorgabe**: 20 Minuten <br>
**Form**: Individuell
**Zusammentragen**: Alle an der Wandtafel
--

## Fragen:

Die meisten Antworten sollten sie im [Repo des Modules m162](https://gitlab.com/ch-tbz-it/Stud/m162) oder ihren Notizen zum Modul 162 finden.

1. Was sind Skalentypen?
2. Welche Datentypen kennen sie?
3. Was fällt ihnen zu Zeichenkodierungen ein?
4. Was fällt ihnen zum Datenmodellen ein?
5. Was sind Beziehungen?
6. Warum gibt es redundanzen in Daten? Was sind die Probleme damit? Wie heisst der Lösungungsprozess des Problems?
   


