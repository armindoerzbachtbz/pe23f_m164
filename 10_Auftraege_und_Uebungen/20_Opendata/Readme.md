# Opendata

[TOC]

Zeit: 45 Min.<br>
Form: 2er Team

Sie haben bereits im [Modul 162](https://gitlab.com/ch-tbz-it/Stud/m162) das Thema [Opendata](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Daten_Formate/OpenData.md) kennengelernt.

In diesem Auftrag geht es nun, Ihr Wissen zu vertiefen und mit Opendata-Quellen zu arbeiten. 

Die Stadt Zürich veröffentlicht Steuerdaten von natürlichen Personen. Gehen Sie auf die [Opendata website der Stadt Zürich](https://data.stadt-zuerich.ch/group/bevolkerung?q=&sort=score+desc%2C+date_last_modified+desc&_tags_limit=0&tags=steuerpflichtige) und laden Sie die aktuellen Steuerdaten (*Median-Einkommen steuerpflichtiger natürlicher Personen nach Jahr, Steuertarif und Stadtquartier*) als CSV-File herunter.

## Aufgaben

1. Analysieren Sie das CSV file und Normalisieren Sie wo nötig. 
2. Legen Sie für die Datenbank-Tabellen die Datentypen fest. Schreiben Sie jeweils ein DDL Skript.
3. Sie haben nun ein ERD bereitgestellt. Importieren Sie nun die Steuerdaten im CSV File in die Datenbank per Bulkimport.
4. Analysieren Sie nun die Daten. Was bedeuten die Felder _p25, _p50, p75? Beantworten Sie.<br>
   Tipp: Es handelt sich um [Grundlagen der Statistik](https://wissenschafts-thurm.de/grundlagen-der-statistik-lagemasse-median-quartile-perzentile-und-modus/).
4.	Lösen folgende Fragen:<br>
    a.	Ermitteln Sie das Quartier mit maximalem Steuereinkommen für _p75.<br>
    b.	Welches Quartier hat das niedrigste Steuereinkommen für _p50?<br>
    c.	Welches Quartier hat das höchste Steuereinkommen für _p50?<br>
