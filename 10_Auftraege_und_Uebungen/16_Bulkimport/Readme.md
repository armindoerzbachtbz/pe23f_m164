# Bulkimport: Mysql Data Loader `LOAD DATA INFILE`

[TOC]

Zeit: 45 Min.<br>
Form: 2er Team

Mysql bietet die Möglichkeit CSV-Files effizient und schnell in eine Tabelle mit gleichen Spalten einzulesen.

Das Kommando dazu heist `LOAD DATA INFILE "/path/file.csv"`. 

Mit diesem Kommando, versucht MariaDB Server, die Eingabedatei aus **seinem eigenen Dateisystem zu lesen**. Ohne Pfadangabe sucht der Server die Datei im Data-Verzeichnis der aktuellen Datenbasis: `C:\...\mySQL\data\db\`.

`LOAD DATA LOCAL INFILE "c:\\path\\file.csv"`

Wenn Sie dagegen diese Anweisung ausführen, versucht der **Client, die Eingabedatei aus seinem Dateisystem** zu lesen, und sendet den Inhalt der Eingabedatei an den MariaDB Server. Auf diese Weise können Sie Dateien aus dem lokalen Dateisystem des Clients in die Datenbank laden.

## Settings

Dieser Vorgang kann evtl. aus Sicherheitsgründen im Server und im Client deaktiviert sein!

Der **Server** sollte folgende Einstellung haben:

	SET GLOBAL local_infile=1;
	SHOW GLOBAL VARIABLES LIKE 'local_infile';

Es sollte auch kein spezieller Import-Pfad gesetzt sein! 
	
	SHOW VARIABLES LIKE 'secure_file_priv';
	
Sonst `my.ini` im Abschnitt `[mysqld]` diese Zeile ergänzen  `secure-file-priv = ""`.
   
Der **mySql.exe-Client** sollte folgende Einstellung im `my.ini` haben:

	[mysql]
	safe-updates
	MYSQL_OPT_LOCAL_INFILE=1


Bei **Workbench** muss die "Connection" angepasst werden:
`OPT_LOCAL_INFILE=1` unter `Edit Connection...>Advance>Other` einfügen

![Einstellung](./x_res/WB_Local_Setting.png)


Wenn Sie einen anderen Client oder eine andere Client-Bibliothek benutzen, lesen Sie die Dokumentation für Ihren spezifischen Client oder Ihre Client-Bibliothek, um festzustellen, wie sie die Anweisung `OPT_LOCAL_INFILE=1` handhabt. 



## Auftrag 1

Im [Tutorial](https://www.mysqltutorial.org/import-csv-file-mysql-table/) wird erklärt wie es funktioniert. ([discounts.csv](./x_res/discounts.csv), [discounts_2.csv](./x_res/discounts_2.csv))

In der [offiziellen Mysql-Dokumentation](https://dev.mysql.com/doc/refman/8.0/en/load-data.html) wird das Statement noch in allen Details beschrieben.

Versuchen Sie folgende [CSV File](./x_res/personen.csv) in eine Tabelle einzulesen. Untersuchen Sie die CSV-Datei zuerst: Trennzeichen(Delimiter), ", Spaltennamen, Zeichensatz, Datumformat, ...

## Vorgehen

- Erzeugen Sie mit einem SQL-Statement eine Datenbank mit dem Namen "PeronenDB_ihrnahme"
- Erzeugen Sie mit einem SQL-Statement eine Tabelle mit den Spalten Vorname, Nachname und Geburtsdatum (InnoDB, Zeichensatz)

- Benutzen Sie das `LOAD DATA LOCAL INFILE ....` um das [Personen CSV File](./x_res/personen.csv) zu importieren.
  - Hinweis: Wenn Ihre Datenbank in **docker** läuft, müssen Sie nach dieser [Anleitung](anleitung%20bulkimport%20mit%20docker.md) das Statement ausführen.
- Kontrollieren Sie ob 500 Zeilen korrekt importiert wurden.
- Zum Schluss löscht ihr die Datenbank mit einem SQL-Statement

## Auftrag 2

Versuchen Sie auch weitere 100 DS aus dem [Personen_DE File](./x_res/personen_de.csv) in die Datenbank-Tabelle einzulesen.

Warum gibt es Fehler? 

Einstellungen siehe Tutorial oben.
