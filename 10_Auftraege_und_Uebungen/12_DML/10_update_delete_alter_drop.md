# Update, delete, alter und drop

Zeit: 30 Min.<br>
Form: 2er Team

Für diese Übung verwenden Sie die [Filme Datenbank](../90_Dumps/filmeDatenbank_dump.sql)

## Aufgaben

Alle Ihre geschriebenen Statements müssen in einem separaten Skript gespeichert werden!
Achten Sie darauf, dass das Skript [idempotent](https://www.itwissen.info/Idempotent-idempotent.html) geschrieben ist.

1.	Beim Regisseur «Cohen» fehlt der Vorname. Vervollständigen sie den Regisseur Namen mit dem Vornamen «Etan». 
2.	Der Film «Angst» dauert nicht 92 Minuten, sondern 120 Minuten. Korrigieren Sie.
3.	DVD gibt es nicht mehr. Das Sortiment wurde durch «Bluray» Medien ersetzt. Nennen Sie die Tabelle um nach «bluray_sammlung».
4.	Eine neue Spalte «Preis» soll hinzugefügt werden.
5.	Der Film «Angriff auf Rom» von Steven Burghofer wurde aus dem Sortiment entfernt. Bereinigen Sie die Tabelle.
6.	Die Spalte «filme» soll nach «kinofilme» umbenannt werden.
7.	Die Spalte Nummer wird nicht mehr benötigt. Löschen Sie sie.
8.	Der Filmverleih rentiert nicht mehr. Die Firma wurde geschlossen und folglich werden alle Daten eliminiert. Löschen Sie die Tabelle.
