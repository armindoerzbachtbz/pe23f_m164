![TBZ Logo](../x_res/tbz_logo.png)

[TOC]

# m164 - Installation, Manuals & Tutorials


## 1) MySQL-Workbench (m162 - WIN macOS)

Für das Absetzen der SQL-Statements verwenden wir vorzugsweise das im m162 installierte **MySQL-Workbench**: [https://dev.mysql.com/downloads/workbench/](https://dev.mysql.com/downloads/workbench/)


### Workbench Manual & Tutorials:
[MySQL Workbench Manual](https://dev.mysql.com/doc/workbench/en/)

[Youtube MySQL Workbench Tutorial](https://www.youtube.com/watch?v=X_umYKqKaF0)

[Youtube MySQL innodb workbench Referentielle Integrität](https://www.youtube.com/watch?v=Y6ymh8MSkxQ)

---

## 2) MySQL Server installieren
Sie haben verschiedene Möglichkeiten einen MySql Server zu installieren. Es kann auch sein, dass eine Lehrperson eine Variante vorgibt

- Wenn Sie mit **XAMPP** arbeiten möchten, verwenden Sie [diese Anleitung](./XAMPP/README.md). XAMPP installiert Ihnen Software direkt auf Ihrem Rechner und kann so die Leistung beeinflussen.

- Falls Sie aber mit **docker** arbeiten möchten, folgen Sie den Anweisungen dieser [Anleitung](Docker/Readme.md). Zwar wird Docker ebenfalls lokal ausgeführt, aber Sie können die Umgebung einfacher kontrollieren und starten/stoppen.
- Die dritte Variante ist **AWS Cloud**. Hier erstellen Sie einen virtuellen Server in der public cloud von Amazon. Folgen Sie dazu [dieser Anleitung]((./AWSCloud/Readme.md)). Ihr lokales System wird nicht beeinflusst, aber Sie benötigen eine Internetverbindung während Sie arbeiten.


---


##	3) Datenbank mit mySQL Workbench verbinden

Ihre Datenbank ist nun lokal installiert und betriebsbereit.

Öffnen Sie nun mySQL workbench und gehen Sie auf die Startseite.

![](../x_res/workbench_start.png)

Erstellen Sie nun eine neue mySQL Verbindung, indem Sie auf «PLUS» klicken.

![](../x_res/workbench_connection.png)

Ein neues Fenster erschein. Geben Sie nun Ihren eigenen Connection Name ein und ändern Sie den Port auf 3306 (Standardport) für XAMPP oder AWS Installation und 3307 für die Docker Installation.

![](../x_res/workbench_connection_port.png)

Machen Sie nun eine «Test Connection» und geben Sie das root Passwort «password» ein.

![](../x_res/workbench_password.png)

Nachdem Sie Ihre Eingabe bestätigt haben, erscheint nun diese Meldung. Sie haben sich nun erfolgreich mit ihrer lokalen Datenbank verbunden!

![](../x_res/workbench_connection_successful.png)

Ihre Datenbank steht nun zur Verfügung. Nun sind Sie bereit, ihr eigenes **Modell** in der Datenbank, bzw. **SQL-Befehle** im Editor zu erstellen.

![](../x_res/workbench_db_ready1.png)<br>
![](../x_res/workbench_db_ready2.png)







