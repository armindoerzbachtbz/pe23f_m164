# MySQL in AWS Cloud installieren



Folgen Sie dieser [allgemeinen Anleitung](https://gitlab.com/ch-tbz-it/Stud/allgemein/awsinstanzeinfach/-/blob/main/installation.md). Verwenden Sie folgende Einstellungen:

- Name: Geben Sie einen sinnvollen Name wie "m164 SQL Server".
- Betriebssystem: Ubuntu mit der Version 22.04
- Instanz-Typ: t2-Medium
- Schlüsselpaar: vockey (Standardauswahl)
- Speichergrösse: 30GB
- Konfigurationsskript: Verwenden Sie [diesen Inhalt](./awskonfig.yaml) und kopieren Sie ihn in das entsprechende Feld.



Nachdem Ihre Instanz läuft öffnen Sie folgende Ports [unter Verwendung dieser Anleitung](https://gitlab.com/ch-tbz-it/Stud/allgemein/awsinstanzeinfach/-/blob/main/firewall.md):

- 3306: Dies ist der Port für den Datenbankzugriff

- 22: Dies ist der Port für die SSH Konsole.

- 80: Dies ist der Port für den Webzugriff (Adminer)



