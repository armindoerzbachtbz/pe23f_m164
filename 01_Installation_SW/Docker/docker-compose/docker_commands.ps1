﻿docker --help
docker images
docker ps
docker run --name=mysql1 -d mysql/mysql-server:latest

docker pull mysql:latest

docker logs mysql1 2>&1


docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:latest

#1. Image holen
docker pull mysql:latest

#2. Image check
docker images

#3. image starten als container
#läuft mit richtiger Port Angabe
docker run --name mysql_qa -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -d mysql:latest

#4. container check
docker ps