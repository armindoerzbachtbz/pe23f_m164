# DDL (Data Defintion Language)

[TOC]

# Konzept

![KOnzept](./DDL.png)

## Forward Engineering
Forward Engineering bezieht sich auf den Prozess der Erstellung einer Datenbank aus einem Datenmodell. Das Datenmodell kann in einem Diagramm- oder Textformat vorliegen, das die Tabellen, Spalten, Beziehungen und Einschränkungen der Datenbank beschreibt.

Der Forward Engineering-Prozess beginnt mit der Erstellung eines **konzeptionellen Datenmodells**. In einem Datenbank-Design-Tool wie beispielsweise MySQL Workbench wird dieses Modell in ein **logisches Datenmodell** überführt. In diesem Tool können Sie das **Schema** der Datenbank verfeinern, indem Sie Entitäten und Attribute hinzufügen und Beziehungen zwischen diesen erstellen.  (Siehe Modul 162)

Durch die Definition der benutzten, DB-Engine-abhängigen Datentypen erhalten Sie nun das **physikalische Modell** zur Implementierung in das ausgewählte Datenbankmanagementsystems (DBMS). 

Dazu wird normalerweise ein ganzes **SQL-Skript** erstellt, das die CREATE-Anweisungen für die Tabellen und Einschränkungen (constraints) enthält ([DDL](https://www.w3schools.in/mysql/ddl-dml-dcl)). Dieses Skript wird dann im DBMS (MariaDB, MySQL, Oracle usw.) ausgeführt, um die Struktur der Datenbasis zu erstellen.

Der Vorteil von Forward Engineering besteht darin, dass es den Prozess der Datenbankerstellung automatisiert und Fehler reduziert. Indem das Datenmodell zuerst erstellt und dann in eine Datenbank umgewandelt wird, können Probleme und Inkonsistenzen frühzeitig erkannt werden, bevor die Datenbank im Einsatz ist.

Zusammenfassend ist Forward Engineering ein wichtiger Schritt im Prozess der Erstellung einer SQL-Datenbank aus einem Datenmodell. Es automatisiert den Prozess der Datenbankerstellung und minimiert menschliche Fehler, indem es das Datenmodell zuerst erstellt und dann in eine Datenbank umwandelt.

**Auftrag**: Setzen Sie den [Auftrag Forward Engineering](../../10_Auftraege_und_Uebungen/10_DDL/03_forward_engineering.md) um.

<br/>

# DDL-Statements

## CREATE DATABASE / SCHEMA

Siehe auch Kapitel 2 unter [Skript\_M164\_Themenübersicht](02_Theorie_Unterlagen/Skript_M164_Themenuebersicht.pdf)

CREATE SCHEMA ist eine SQL-Anweisung, die verwendet wird, um eine neue Datenbank in der SQL-Datenbank zu erstellen. Eine SQL-Datenbank ist im Grunde genommen eine Sammlung von Tabellen, die zusammengehören und auf die gleiche Weise verwaltet werden. CREATE SCHEMA erstellt also eine leere Datenbank, in der Tabellen erstellt und Daten gespeichert werden können.

Die Syntax von CREATE SCHEMA ist wie folgt:

```sql
CREATE SCHEMA db_name;
```

Hier ist "db_name" der Name der Datenbank, die erstellt werden soll. Wenn Sie eine Datenbank erstellen, wird normalerweise der Standard-Charset und die Standard-Kollation verwendet. Diese können jedoch in der CREATE SCHEMA-Anweisung auch explizit angegeben werden:

```sql
CREATE SCHEMA schema_name
  DEFAULT CHARACTER SET utf8mb4
  DEFAULT COLLATE utf8mb4_general_ci;
```

In diesem Beispiel wird eine neue Datenbank namens "schema_name" erstellt, wobei UTF-8 als Zeichensatz (mb4 steht für das 4.Byte für z.B. Emojis) und "utf8mb4_general_ci" als [Kollation](https://mariadb.com/kb/en/setting-character-sets-and-collations/) verwendet wird. (Hinweis: Kollation = Art wie sortiert wird. _ci = case insensitive, _cs = case sensitive)

Sobald die Datenbank erstellt wurde, können Sie mit CREATE TABLE-Anweisungen Tabellen in der Datenbank erstellen und mit INSERT-Anweisungen Daten in die Tabellen einfügen.

Es ist zu beachten, dass CREATE SCHEMA in SQL synonym zu CREATE DATABASE ist. CREATE SCHEMA und CREATE DATABASE können daher austauschbar verwendet werden. Beide Anweisungen führen letztendlich zur Erstellung einer neuen Datenbank in SQL.

Das ganze soll nun **MANUELL** mit dem Klienten Workbench nachvollzogen werden:

![WB-SQL-Editor](./SQL-Script_WB.png)

**Auftrag**: Setzen Sie den [Auftrag Create Schema](../../10_Auftraege_und_Uebungen/10_DDL/03_create_schema.md) um.

<br/>

## CREATE TABLE

[Präsentation CREATE TABLE](create-table.pdf)<br>

Zu einer Tabelle ordnet man Attribute/ Columns zu. Dementsprechend sind [Felddatendypen](Datentypen/Felddatentypen.pdf) zu deklarieren.

CREATE TABLE ist eine SQL-Anweisung, die verwendet wird, um eine neue Tabelle in einer SQL-Datenbank zu erstellen. Mit CREATE TABLE können Sie eine Tabelle erstellen und dabei die Spalten, Datentypen und Einschränkungen definieren, die in der Tabelle enthalten sein sollen.

Die Syntax von CREATE TABLE sieht wie folgt aus:

```sql
CREATE TABLE table_name (
    column1 datatype constraints,
    column2 datatype constraints,
    ...
    column_n datatype constraints
);
```

Hier ist "table_name" der Name der Tabelle, die erstellt werden soll. Jede Spalte wird durch den Namen und den Datentyp definiert. Der Datentyp gibt an, welchen Typ von Daten in der Spalte gespeichert werden soll, z. B. INTEGER, VARCHAR, DATE usw. Sie können auch Einschränkungen definieren, um sicherzustellen, dass die Daten in der Tabelle bestimmte Regeln oder Bedingungen erfüllen, z. B. die Anforderung, dass eine Spalte keine NULL-Werte enthalten darf oder dass ein eindeutiger Wert in einer Spalte vorhanden sein muss.

Ein Beispiel für eine CREATE TABLE-Anweisung könnte wie folgt aussehen:

```sql
CREATE TABLE customers (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    age INT,
    PRIMARY KEY (id)
);
```
In diesem Beispiel wird eine Tabelle "customers" mit vier Spalten erstellt: "id", "name", "email" und "age". Die Spalte "id" ist ein INTEGER und wird als Primärschlüssel verwendet, um die Zeilen in der Tabelle zu identifizieren. Die Spalte "name" und "email" sind beide VARCHAR-Typen und haben die Einschränkung, dass sie nicht NULL sein dürfen. Die Spalte "email" hat auch die Einschränkung UNIQUE, was bedeutet, dass jeder Eintrag in der Spalte eindeutig sein muss. Die Spalte "age" ist optional und hat keinen Einschränkungen.

Insgesamt bietet CREATE TABLE eine flexible und leistungsstarke Möglichkeit, um Tabellen in SQL-Datenbanken zu erstellen und anzupassen.

**Auftrag**: Setzen Sie den [Auftrag Create Table](../../10_Auftraege_und_Uebungen/10_DDL/03_create_table.md) um.

<br/>

## DROP TABLE
DROP TABLE ist eine SQL-Anweisung, die verwendet wird, um eine Tabelle in der SQL-Datenbank zu löschen. Wenn Sie eine Tabelle mit DROP TABLE löschen, werden alle Daten in der Tabelle permanent entfernt und können nicht wiederhergestellt werden. Es ist daher wichtig, sicherzustellen, dass Sie die richtige Tabelle löschen, bevor Sie DROP TABLE ausführen.

Die Syntax von DROP TABLE sieht wie folgt aus:

```sql
DROP TABLE table_name;
```

Hier ist "table_name" der Name der Tabelle, die gelöscht werden soll. Wenn die Tabelle erfolgreich gelöscht wird, gibt die SQL-Datenbank eine Bestätigungsmeldung zurück.

Es ist auch möglich, mehrere Tabellen in einer DROP TABLE-Anweisung zu löschen, indem Sie einfach die Namen der Tabellen durch Kommas trennen:

```sql
DROP TABLE table_name1, table_name2, table_name3;
```

Es gibt auch zusätzliche Optionen, die in DROP TABLE verwendet werden können, um das Verhalten der Anweisung anzupassen. Zum Beispiel können Sie die Option IF EXISTS verwenden, um sicherzustellen, dass SQL keine Fehlermeldung zurückgibt, wenn die Tabelle, die Sie löschen möchten, nicht existiert:

```sql
DROP TABLE IF EXISTS table_name;
```

Insgesamt bietet DROP TABLE eine einfache Möglichkeit, um unerwünschte Tabellen in der SQL-Datenbanken zu entfernen und Speicherplatz freizugeben. Es ist jedoch wichtig, vorsichtig zu sein und sicherzustellen, dass Sie nur die Tabellen löschen, die Sie tatsächlich löschen möchten.

**Auftrag**: Setzen Sie den [Auftrag drop statement](../../10_Auftraege_und_Uebungen/10_DDL/03_drop_statement.md) um.

<br/>

## ALTER TABLE

Der Befehl ALTER TABLE wird verwendet, um die **Struktur** einer vorhandenen Tabelle zu ändern. Mit diesem Befehl können Sie eine oder mehrere Spalten hinzufügen, ändern oder entfernen, den Datentyp einer Spalte ändern, einen neuen Index hinzufügen oder löschen und viele andere Änderungen an der Tabelle vornehmen.

Die Syntax für die Verwendung von ALTER TABLE lautet wie folgt:

```sql
ALTER TABLE table_name action;
```

"table_name" ist der Name der Tabelle, die geändert werden soll, und "action" ist die zu ändernde Aktion. Hier sind einige Beispiele für häufige Aktionen, die mit ALTER TABLE durchgeführt werden können:

**ADD** fügt eine Spalte hinzu:

```sql
ALTER TABLE table_name ADD column_name column_definition;
```

**RENAME COLUMN** verändert den Namen einer Spalte:

```sql
ALTER TABLE table_name RENAME COLUMN old_column_name TO new_column_name;
```

>
> Hinweis: Dieser Befehl geht erst ab MariaDB Version 10.5.2! Evtl. SQL-Server [updaten](../../01_Installation_SW#mariadb-upgraden)!
> 


**CHANGE** ist der Befehl, um eine Spalte zu ändern, sowohl den Namen als auch ihre Definition. Beim Befehl gibt man den *alten* und *neuen* Namen sowie die *neue Definition* an. Wenn z.B. eine INT NOT NULL-Spalte von old_column_name in new_column_name umbenennen wollen und die Definition so geändert wird, dass sie den BIGINT-Datentyp verwendet, während das NOT NULL-Attribut beibehalten wird, gehen Sie wie folgt vor:

```sql
ALTER TABLE table_name CHANGE old_column_name new_column_name BIGINT NOT NULL;
```

**MODIFY** ist die elgegantere Variante, wenn der Name gleich bleibt aber die Definition ändert. Der Spaltenname muss deshalb nur einmal angegeben werden:

```sql
ALTER TABLE table_name MODIFY column_name new_data_type;
```

**DROP** ist der Befehl, zum Entfernen einer Spalte:

```sql
ALTER TABLE table_name DROP column_name;
```

Es gibt viele weitere Optionen, die mit ALTER TABLE durchgeführt werden können, um die Struktur einer Tabelle zu ändern. Es ist jedoch wichtig zu beachten, dass einige Änderungen, wie das Hinzufügen oder Entfernen von Spalten, die bereits Daten in der Tabelle enthalten, zu Datenverlusten führen können, wenn die Änderungen nicht ordnungsgemäß durchgeführt werden.

**Auftrag**: Setzen Sie den [Auftrag alter table](../../10_Auftraege_und_Uebungen/10_DDL/05_alter_table.md) um.

---

Quellen MySQL:<br>
https://dev.mysql.com/doc/refman/8.0/en/create-database.html<br>
https://dev.mysql.com/doc/refman/8.0/en/create-table.html<br>
https://dev.mysql.com/doc/refman/8.0/en/drop-table.html<br>
https://dev.mysql.com/doc/refman/8.0/en/alter-table.html<br>
https://dev.mysql.com/doc/refman/8.0/en/charset-general.html
