# DDL CREATE -Relations-

[TOC]

## Beziehungen erstellen

Im logischen (und physischen) Datenmodell sind die Beziehungen der relationalen Datenbank aufgrund der möglichen Einstellungen (constraints) der Fremdschlüssel eingeschränkt!

Folgende Beziehungen können realisiert werden im physischen Modell:

| Beziehung <br> *MasterTab.links : DetailTab.rechts* | möglich | **nicht möglich<sup>1</sup>** &#10132; wird zu | Constraints FK |
|-----------|---------|---------------------------|-------------|
|eins zu eins <br>  | 1:c <br> c:c | **1:1** &#10132; 1:c <br> -| NN UQ <br> -- UQ |
|eins zu viele <br>  | 1:mc <br> c:mc | **1:m** &#10132; 1:mc <br> **c:m** &#10132; c:mc| NN -- <br> -- -- |
|viele zu viele <br> nur via Transformationstabelle |  | **m:m, m:mc, mc:m, mc:mc** <br> &#10132; 1:mc-[TT]-mc:1 | - <br> NN -- & NN -- |

> <sup>1</sup>Gewisse Beziehungen, die der Kunde wünscht, können in einem RDBMS also nicht realisiert werden. Bzw. sie können nicht *genau* realisiert werden, sondern je nach Fall wird 1 zu c und in jedem Fall wird m zu mc, siehe Tabelle oben.
Damit den Kundenwünschen trotzdem entsprochen werden kann, müssen die Regeln via Applikation (z.B. GUI) sichergestellt werden. Das ist aber nicht Bestandteil dieses Moduls. 

## Forward Engineering

Wir wollen nun untersuchen, wie Workbench die DDL-SQL-Befehle für die vier möglichen Beziehungstypen erzeugt! 

1. Laden Sie die Datei [BeziehungenLE.mwb](./BeziehungenLE.mwb) herunter und öffnen Sie diese mit Workbench.
2. Lesen Sie das unten angezeigte ERD und analysieren Sie die Tabellen und Beziehungen. Setzen Sie das Häkchen in den *Preferences>Modeling>Relationships>Show Caption*
3. Erstellen Sie die vier angegebenen Beziehungen und setzen Sie die Eigenschaften (constraints) der Fremdschlüssel gemäss obiger Tabelle richtig. <br> ![VierBeziehungen.png](./x_res/VierBeziehungen.png)
4. Erzeugen Sie mittels Forward Engineering das DDL-SQL-Script und speichern Sie es ab.

> Hinweis: Die Rolle/Bezeichnung der Beziehung kann in Workbench durch einen Eintrag im **Relationship>>Caption** Feld erfolgen!

### Analyse (Partnerarbeit)

Untersuchen Sie das erzeugte Script und beantworten Sie folgende Fragen:

1. Erstellen Sie einen allgemeinen Syntax für die CONSTRAINT-Anweisung.
2. Wie wird beim Fremdschlüssel der Constraint NOT NULL erstellt?
3. Weshalb wird für jeden Fremdschlüssel ein Index erstellt? Lesen Sie [hier](https://www.datenbanken-verstehen.de/datenmodellierung/datenbank-index/)!
4. Wie wird der Constraint UNIQUE für ein Fremdschlüssel in Workbench mit Forward Engineering erstellt?

<br>
<br>
<br>

> Hinweis: Anstelle des UNIQUE-Index kann nur der Fremdschlüssel auf UNIQUE gesetzt werden: <br> `FK_Fahrer INT NULL UNIQUE,` <br> Es wird aber trotzdem ein Index erstellt, um die Einmaligkeit des Fremdschlüssels effizient sicherzustellen!

---

## ALTER TABLE <> ADD CONSTRAINT <> FOREIGN KEY ...
Mit ALTER TABLE können Fremdschlüssel manuell auch nachträglich eingefügt werden.  

Fremdschlüssel hinzufügen:


```sql

ALTER TABLE <DetailTab>
  ADD CONSTRAINT <Constraint> FOREIGN KEY (<Fremdschlüssel>)
  REFERENCES <MasterTab> (Primärschlüssel);
  
```


| `<Wert>` | Erklärung |
|---|---| 
| `<DetailTab>` | Name der Detailtabelle |
| `<Constraint>` | Frei definierbarer Name. Mögliche Namenskonvention: FK\_Detailtabelle\_Mastertabelle, z.B. FK\_Autoren\_Ort. |
| `<Fremdschlüssel>` | Name des Fremdschlüsselattributes der Detailtabelle |
| `<MasterTab>` | Name der Master-/Primärtabelle |
| `<Primärschlüssel>` | Name des Primärschlüsselattributes der Master-/Primärtabelle |


Eindeutiger Schlüssel hinzufügen (z.B. darf jeder Fremdschlüsselwert max. 1x vorkommen):


```sql

ALTER TABLE <Tabelle>
  ADD UNIQUE (<Spalte>); 
  
```

---

### Für Fortgeschrittene:

Fügen Sie Daten in die Datensätze ein und überprüfen Sie die Beziehungen. Was geschieht, wenn Sie z.B. bei einer 1:c Beziehung zwei gleiche Fremdschlüsselwerte angeben? 

