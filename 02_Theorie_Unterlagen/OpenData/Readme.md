# Opendata einbinden


Sie haben bereits im [Modul 162](https://gitlab.com/ch-tbz-it/Stud/m162) das Thema [Opendata](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Daten_Formate/OpenData.md) kennengelernt.

Im Folgenden soll gezeigt werden, wie man eine (OpenData-) Datentabelle in eine normalisierte Datenbank einlesen kann, um sie dann als Quelle für Abfragen nutzen zu können!


## Demo LP Freifächer

![Intro](x_res/1.png)

![Intro](x_res/2.png)

![Intro](x_res/3.png)

--> Einlesen in Tabellen gemäss Farben (ID=FK)

![Intro](x_res/4.png)

![Intro](x_res/5.png) ![Intro](x_res/6.png)

**--> DML anwenden. Doppelte Einträge ignorieren!**

---
### Ressourcen

[Komplettes EXCEL](./Freifaecher_OpenData_Demo.xlsx)

[.mwb für Forward Engineering](./Freifaecher.mwb)


---

>**Hinweis** Testen Sie die eingelesenen Daten! Z.B. lesen Sie alle Daten in **EINE Tabelle** ein und erstellen Sie ein paar **Stichproben** mit Select-Abfragen. Vergleichen Sie diese Stichproben dann mit den gleichen, angepassten **Select-Abfragen aus ihrer normalisierten Datenbank**. Die Resultate sollten dieselben sein!
