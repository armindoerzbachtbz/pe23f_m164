-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';

-- -----------------------------------------------------
-- Schema freifaecher
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `freifaecher` ;

-- -----------------------------------------------------
-- Schema freifaecher
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `freifaecher` DEFAULT CHARACTER SET utf8 ;
USE `freifaecher` ;

-- -----------------------------------------------------
-- Table `freifaecher`.`tbl_LehrerIn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `freifaecher`.`tbl_LehrerIn` ;

CREATE TABLE IF NOT EXISTS `freifaecher`.`tbl_LehrerIn` (
  `ID_LehrerIn` INT NOT NULL,
  `Vorname` VARCHAR(30) NULL,
  `Nachname` VARCHAR(30) NULL,
  PRIMARY KEY (`ID_LehrerIn`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `freifaecher`.`tbl_Klasse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `freifaecher`.`tbl_Klasse` ;

CREATE TABLE IF NOT EXISTS `freifaecher`.`tbl_Klasse` (
  `ID_Klasse` INT NOT NULL,
  `Klasse` VARCHAR(30) NOT NULL,
  `FK_KlassenLehrerIn` INT NOT NULL,
  PRIMARY KEY (`ID_Klasse`),
  CONSTRAINT `fk_tbl_Klasse1`
    FOREIGN KEY (`FK_KlassenLehrerIn`)
    REFERENCES `freifaecher`.`tbl_LehrerIn` (`ID_LehrerIn`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_tbl_Klasse1_idx` ON `freifaecher`.`tbl_Klasse` (`FK_KlassenLehrerIn` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `freifaecher`.`tbl_SchülerIn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `freifaecher`.`tbl_SchülerIn` ;

CREATE TABLE IF NOT EXISTS `freifaecher`.`tbl_SchülerIn` (
  `ID_SchülerIn` INT NOT NULL,
  `Vorname` VARCHAR(30) NULL,
  `Nachname` VARCHAR(30) NULL,
  `FK_Klasse` INT NOT NULL,
  PRIMARY KEY (`ID_SchülerIn`),
  CONSTRAINT `fk_tbl_SchülerIn1`
    FOREIGN KEY (`FK_Klasse`)
    REFERENCES `freifaecher`.`tbl_Klasse` (`ID_Klasse`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_tbl_SchülerIn1_idx` ON `freifaecher`.`tbl_SchülerIn` (`FK_Klasse` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `freifaecher`.`tbl_Freifach`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `freifaecher`.`tbl_Freifach` ;

CREATE TABLE IF NOT EXISTS `freifaecher`.`tbl_Freifach` (
  `ID_Freifach` INT NOT NULL,
  `Freifach` VARCHAR(30) NULL,
  `FK_FF_LehrerIn` INT NOT NULL,
  PRIMARY KEY (`ID_Freifach`),
  CONSTRAINT `fk_tbl_Freifach1`
    FOREIGN KEY (`FK_FF_LehrerIn`)
    REFERENCES `freifaecher`.`tbl_LehrerIn` (`ID_LehrerIn`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_tbl_Freifach1_idx` ON `freifaecher`.`tbl_Freifach` (`FK_FF_LehrerIn` ASC) VISIBLE;

CREATE UNIQUE INDEX `FK_FF_LehrerIn_UNIQUE` ON `freifaecher`.`tbl_Freifach` (`FK_FF_LehrerIn` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `freifaecher`.`TT_Teilnahme`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `freifaecher`.`TT_Teilnahme` ;

CREATE TABLE IF NOT EXISTS `freifaecher`.`TT_Teilnahme` (
  `ID_Teilnahme` INT NOT NULL,
  `Tag` VARCHAR(2) NULL,
  `Zimmer` VARCHAR(10) NULL,
  `FK_Freifach` INT NOT NULL,
  `FK_SchülerIn` INT NOT NULL,
  PRIMARY KEY (`ID_Teilnahme`),
  CONSTRAINT `fk_tt_Teilnahme1`
    FOREIGN KEY (`FK_Freifach`)
    REFERENCES `freifaecher`.`tbl_Freifach` (`ID_Freifach`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_tt_Teilnahme2`
    FOREIGN KEY (`FK_SchülerIn`)
    REFERENCES `freifaecher`.`tbl_SchülerIn` (`ID_SchülerIn`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_tt_Teilnahme1_idx` ON `freifaecher`.`TT_Teilnahme` (`FK_Freifach` ASC) VISIBLE;

CREATE INDEX `fk_tt_Teilnahme2_idx` ON `freifaecher`.`TT_Teilnahme` (`FK_SchülerIn` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
