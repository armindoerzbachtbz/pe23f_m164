-- DML CSV-einlesen - Reihenfolge beachten!
use freifaecher;

-- LehrerIn ohne FK 
LOAD DATA INFILE 'tbl_LehrerIn.csv' -- C:\XAMPP\...\DATA\FREIFAECHER
    IGNORE
	INTO TABLE tbl_LehrerIn
	FIELDS TERMINATED BY ';'      -- Delimiter
    LINES TERMINATED BY '\r\n'    -- Windows
    ENCLOSED BY '"'               -- Werte sind in " eingeschlossen
	IGNORE 1 LINES;               -- Titelzeile ignorieren

Truncate tbl_Klasse;
-- Klasse --> LehrerIn
LOAD DATA INFILE 'tbl_Klasse.csv' -- C:\XAMPP\...\DATA\FREIFAECHER
    IGNORE
	INTO TABLE tbl_Klasse
	FIELDS TERMINATED BY ';' 
    LINES TERMINATED BY '\r\n'    -- Windows
	IGNORE 1 LINES; -- Titelzeile ignorieren
    
-- SchülerIn -->Klasse
LOAD DATA INFILE 'tbl_SchülerIn.csv' -- C:\XAMPP\...\DATA\FREIFAECHER
    IGNORE
	INTO TABLE tbl_SchülerIn
	FIELDS TERMINATED BY ';' 
    LINES TERMINATED BY '\r\n'    -- Windows
	IGNORE 1 LINES; -- Titelzeile ignorieren    

-- Freifach -->Zimmer, LehrerIn
LOAD DATA INFILE 'tbl_Freifach.csv' -- C:\XAMPP\...\DATA\FREIFAECHER
    IGNORE
	INTO TABLE tbl_Freifach
	FIELDS TERMINATED BY ';' 
    LINES TERMINATED BY '\r\n'    -- Windows
	IGNORE 1 LINES; -- Titelzeile ignorieren  

-- Teilnahme --> Freifach, SchülerIn
LOAD DATA INFILE 'tbl_Teilnahme.csv' -- C:\XAMPP\...\DATA\FREIFAECHER
    IGNORE
	INTO TABLE tt_Teilnahme
	FIELDS TERMINATED BY ';' 
    LINES TERMINATED BY '\r\n'    -- Windows
	IGNORE 1 LINES; -- Titelzeile ignorieren  
    
    
-- Ganze Listen ausgeben zu Testzwecken
Select l.ID_LehrerIn, l.nachname, l.vorname, f.Freifach
FROM tbl_Lehrerin AS l LEFT JOIN tbl_Freifach AS f ON f.FK_FF_Lehrerin = l.ID_LehrerIn
ORDER BY l.ID_LehrerIn;

Select s.Nachname AS "SchülerIn Nachname", s.Vorname AS "SchülerIn Vorname", k.Klasse, l.nachname AS "LehrerIn Nachname", l.vorname AS "LehrerIn Vorname", f.Freifach, t.Tag, t.Zimmer
FROM tbl_schülerin as s JOIN tbl_Klasse as k ON s.FK_KLasse = k.ID_Klasse
JOIN tbl_Lehrerin AS l ON k.FK_KlassenLehrerin = l.ID_LehrerIn
JOIN tt_Teilnahme AS t ON t.FK_SchülerIn = s.ID_SchülerIn
JOIN tbl_Freifach AS f ON t.FK_Freifach = f.ID_Freifach
ORDER BY s.ID_SchülerIn;
