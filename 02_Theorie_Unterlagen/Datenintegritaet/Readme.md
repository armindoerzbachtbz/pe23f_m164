# Datenintegrität

Datenintegrität in einer Datenbank bezieht sich auf die Genauigkeit, Konsistenz und Vollständigkeit der in der Datenbank gespeicherten Daten. Sie stellt sicher, dass die Daten korrekt sind und nicht versehentlich oder absichtlich verändert werden.

Es gibt verschiedene Aspekte der Datenintegrität in einer Datenbank, einschließlich:

1. Eindeutigkeit/Datenkonsistenz: Jeder Datensatz in der Datenbank sollte eindeutig identifizierbar sein, um zu verhindern, dass Daten doppelt eingegeben werden (Redundanzen).

2. Referenzielle Integrität: Wenn eine Tabelle Beziehungen zu anderen Tabellen hat, sollten die Beziehungen immer konsistent bleiben. Das bedeutet, dass z.B. eine Verknüpfung zwischen einer Bestellung und einem Kunden nur dann bestehen kann, wenn der Kunde auch tatsächlich in der Kundentabelle vorhanden ist.

3. Datentypen: Die Daten sollten in der Datenbank in den korrekten Datentypen gespeichert werden, um sicherzustellen, dass sie korrekt behandelt werden können. Beispielsweise sollte eine Telefonnummer in einem Feld vom Typ "string" (Zeichenfolge) und nicht vom Typ "integer" (Ganzzahl) gespeichert werden.

4. Datenbeschränkungen: Datenbeschränkungen stellen sicher, dass die Daten in der Datenbank gültig sind. Zum Beispiel kann eine Datenbank so eingerichtet werden, dass nur positive Zahlen in einem bestimmten Feld eingegeben werden dürfen, oder dass eine E-Mail-Adresse nur in einem bestimmten Format eingegeben werden kann.

5. Validierung: Vor dem Einfügen von Daten in die Datenbank sollte eine Validierung durchgeführt werden, um sicherzustellen, dass die Daten den Anforderungen der Datenbank entsprechen.

Insgesamt ist die Datenintegrität ein wichtiger Aspekt einer Datenbank, da sie sicherstellt, dass die Daten in der Datenbank korrekt sind und somit zuverlässige Informationen bereitgestellt werden können.

Diese [Präsentation](Datenintegritaet_in_DB.pdf) fasst das Thema in Kürze zusammen und erklärt Ihnen zusätzlich die **Änderungs- und Löschweitergabe** in einer Datenbank.

## Fremdschlüssel-Regeln beim Löschen
Was passiert, wenn wir einen FOREIGN KEY-Constraint haben und ein Datensatz in der Primärtabelle gelöscht wird? Zu diesem Zweck gibt es **ON DELETE**-Regeln:

| DELETE | Regel | Bedeutung |
|---|---|---|
| ON DELETE | NO ACTION | Ein DELETE in der Primärtabelle kann nur ausgeführt werden, wenn in keiner Detailtabelle ein Fremdschlüssel mit dem entsprechenden Wert existiert. <br> *Dies ist das Standardverfahren, wenn nichts angegeben wird.* |
| | CASCADE | Ein DELETE in der Primärtabelle führt auch zu einem Löschen der entsprechenden Datensätze in der Fremdschlüsseltabelle. <br> **Achtung mit dieser Regel, evtl. werden unbeabsichtigt Daten gelöscht!** |
| | SET NULL | Bei einem DELETE in der Primärtabelle werden die entsprechenden Datensätze in der Fremdschlüsseltabelle auf NULL gesetzt. <br> **Hinweis: Diese Einstellung geht nur bei c:m(c) und c:c Beziehungen, wenn also beim FK NULL erlaubt ist!** |
| | DEFAULT | Bei einem DELETE in der Primärtabelle werden die entsprechenden Datensätze in der Fremdschlüsseltabelle auf den DEFAULT-Wert gesetzt, falls einer definiert worden ist. Ansonsten werden sie auf NULL gesetzt (bei c:m(c) und c:c Beziehungen)|


Neben den ON DELETE-Regeln gibt es auch die **ON UPDATE**-Regeln. Diese kommen zum Zug, wenn ein Primärschlüsselwert ändert. Jedoch lassen wir die Primärschlüsselwerte (ohne weiteren Informationsgehalt) vom System verwalten (AUTO INCREMENT). Die Werte ändern nie, deshalb sind ON UPDATE-Regeln für uns irrelevant.

> Hinweis: Sie können die Standard-Regeln in den Settings Modeling>Defaults verändern: <br> ON-UPDATE > NO ACTION, ON-DELETE > RESTRICT

<br>


**Auftrag**: Setzen Sie den [Auftrag Referentielle Integrität](../../10_Auftraege_und_Uebungen/15_Datenintegritaet/Referentielle_Datenintegritaet.md) um.<br>
**Auftrag**: Setzen Sie den [Auftrag Referentielle Integrität Fortgeschritten](../../10_Auftraege_und_Uebungen/15_Datenintegritaet/Referentielle_Datentegritaet_Fortgeschritten.md) um.<br>




