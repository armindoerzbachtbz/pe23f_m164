# DQL (Data Query Language)

## Subquery

Ein Subselect in einer MySQL-Datenbank ist eine Abfrage innerhalb einer anderen Abfrage. Es wird verwendet, um Daten aus einer Tabelle abzurufen, die Bedingungen erfüllen, die auf der Grundlage von Daten in einer anderen Tabelle ermittelt werden.

Hier ist ein Beispiel für eine SELECT-Abfrage mit einem [Subselect](../../10_Auftraege_und_Uebungen/90_Dumps/subselect.sql):

```sql
use subselect;
SELECT name, age, country
FROM users
WHERE country IN (SELECT name FROM country WHERE region = 'Europa')
```
In diesem Beispiel werden die Namen und Altersangaben von Benutzern aus der Tabelle "users" abgerufen, die in europäischen Ländern leben. Das Subselect wird verwendet, um die Länder in Europa zu ermitteln, indem es eine Abfrage auf der Tabelle "countries" ausführt und nach dem Eintrag "Europe" in der Spalte "region" sucht.

Das Ergebnis des Subselects ist eine Liste von Ländern, die dann als Bedingung für die WHERE-Klausel in der Hauptabfrage verwendet werden, um die Datensätze zu filtern, die die Bedingung erfüllen: **IN** oder auch **NOT IN** für alle anderen.

<br> 

Das Subselect kann auch als Teil einer JOIN-Abfrage verwendet werden, um Daten aus mehreren Tabellen abzurufen.

Hier ist ein Beispiel mit der Datenbank [northwind](../../10_Auftraege_und_Uebungen/90_Dumps/northwind.sql):

```sql
use northwind;
SELECT customers.ContactName, orders.orderdate
FROM customers
INNER JOIN orders
ON customers.customerid = orders.customerid
WHERE orders.orderdate IN (
  SELECT MAX(orderdate)
  FROM orders
  GROUP BY customerid);
```
In diesem Beispiel werden die Namen der Kunden und ihre letzten Bestelldaten aus der Tabelle "customers" und "orders" abgerufen. Das Subselect wird verwendet, um die neuesten Bestelldaten für jeden Kunden zu ermitteln, indem es eine Abfrage auf der Tabelle "orders" ausführt, die nach dem maximalen Datum gruppiert ist. Das Ergebnis des Subselects ist eine Liste der neuesten Bestelldaten für jeden Kunden.

Die Hauptabfrage verwendet dann die JOIN-Klausel, um die Datensätze aus den Tabellen "customers" und "orders" zu verknüpfen, indem sie den Kunden-IDs in beiden Tabellen entsprechen. Die WHERE-Klausel filtert die Datensätze, die die Bedingung erfüllen, indem sie die neuesten Bestelldaten für jeden Kunden verwendet, die aus dem Subselect stammen.

Auf diese Weise können Sie mit einem Subselect als Teil einer JOIN-Abfrage komplexe Datenabfragen durchführen, die Daten aus mehreren Tabellen kombinieren und filtern.

> Hinweis: Wie in diesem Beispiel sichtbar, werden nicht nur die letzten Daten der einzelnen Kunden ausgegeben, sondern auch die (per Zufall) gleichen Daten von anderen Kunden!

Bearbeiten Sie nun den Auftrag [Subselect](../../10_Auftraege_und_Uebungen/14_DQL/24_select_subquery.md)

---
Quellen: <br>
https://www.mysqltutorial.org/mysql-subquery/<br>
https://dev.mysql.com/doc/refman/8.0/en/subqueries.html