# DQL (Data Query Language)

[TOC]

- [Mengenlehre](Theorie_Mengenlehre.md)
- [SELECT](DQL_SEL.md)
- [SELECT JOIN](DQL_SEL_JOIN.md)
- [SELECT ALIAS](DQL_SEL_ALIAS.md)
- [Aggregatsfunktionen](Aggregatsfunktionen.md)
- [SELECT GROUP BY](DQL_SEL_GROUP_BY.md)
- [SELECT HAVING](DQL_SEL_HAVING.md)
