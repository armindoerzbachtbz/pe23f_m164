# Mengenlehre

[TOC]

## Symbole und Zeichen

| Symbol | Beschreibung |
| ------ | ------------ |
| Gross- und Klein-Buchstaben und ∈, ∉ | Grossbuchstaben bezeichnen eine **Menge**. Im Beispiel unten gibt es die drei Mengen *G* (Grundmenge), *A* und *B*. <br />  Kleinbuchstaben bezeichnen **Elemente**, welche einer Menge zugewiesen sind (zumindest der Grundmenge)<br><br>G={a,b,c,d,e,f}, A={a,b}, B={c,d} <br><br>∈ zeigt, dass ein Elemente in einer Menge enthalten ist, zum Beispiel: <br> *a ∈ A*, *b ∈ A* oder *d ∈ B*<br />∉ zeigt, dass ein Element in einer Menge nicht enthalten ist, zum Beispiel: <br> *a ∉ B* oder *d ∉ A*<br />![Mengenlehre-MengenundElemente](./../../x_res/Mengenlehre-MengenundElemente.png) |
| {} oder Ø | Bezeichnet eine **leere Menge**. Im folgenden Beispiel ist die Menge *A* leer. <br>A={}<br />![Mengenlehre-LeereMenge](./../../x_res/Mengenlehre-LeereMenge.png) |
| ⊂, ⊆   | ⊂ oder ⊆ bedeutet **Teilmenge** von. zum Beispiel: <br> *B ⊂ A*<br />Dies ist der Fall, wenn eine Menge komplett in einer anderen Menge enthalten ist.<br> <br />Für ein Element *x* bedeutet dies, dass wenn *x ∈ B* **auch** *x ∈ A* ist.<br /> ![Mengenlehre-Teilmenge](./../../x_res/Mengenlehre-Teilmenge.png)<br /><br />**Achtung**: Es gibt einen Unterschied zwischen den beiden Zeichen, den wir hier aber ignorieren (echte und unechte Teilmengen). |
| ∩      | ∩ bezeichnet die **Schnittmenge** zwischen zwei Mengen, zum Beispiel: <br> *A ∩ B*. <br> Wenn zwei Mengen sich nicht überlagern, ist die Schnittmenge die leere Menge.<br /><br>Für ein Element *x* bedeutet dies, dass *x ∈ B* **und** *x ∈ A* ist.<br />![Mengenlehre-Schnittmenge](./../../x_res/Mengenlehre-Schnittmenge.png) |
| ∪      | ∪ bezeichnet die **Vereinigungsmenge** von zwei Mengen, zum Beispiel: <br> *A ∪ B*. <br> Die beiden Mengen müssen sich nicht überlagern.<br /><br>Für ein Element x bedeutet dies, dass *x ∈ A* **oder** *x ∈ B* ist.<br />![Mengenlehre-Vereinigungsmenge](./../../x_res/Mengenlehre-Vereinigungsmenge.png) |
| X<sup>c</sup>    | X<sup>c</sup> bezeichnet die **Komplementärmenge**, also alle Elemente, die nicht in der Menge X enthalten sind, zum Beispiel: <br> A<sup>c</sup> (alle Elemente im rot markierten Bereich).<br /><br> Für ein Element x bedeutet dies, dass *x ∈ G* **und** *x ∉ A* ist.<br />![Mengenlehre-Komplement](./../../x_res/Mengenlehre-Komplement.png) |
| \ | \ bezeichnet die **Differenzmenge** zwischen zwei Mengen, zum Beispiel <br> *B\A*. <br> Es gehören alle Elemente dazu, die in der Menge B enthalten sind, aber nicht in der Menge A. <br><br> Für ein Element x bedeutet dies, dass *x ∈ B* **und** *x ∉ A* ist.<br />![Mengenlehre-Differenz](./../../x_res/Mengenlehre-Differenz.png) |

---

## Auftrag

Setzen Sie den [Auftrag Mengenlehre](../../10_Auftraege_und_Uebungen/14_DQL/08_Auftrag_Mengenlehre.md) um.

