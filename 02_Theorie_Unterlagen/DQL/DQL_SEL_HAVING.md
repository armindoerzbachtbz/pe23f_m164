# DQL (Data Query Language)

## Select having

In MySQL wird das HAVING-Statement verwendet, um Ergebnisse von Gruppierungsoperationen zu filtern, die durch das GROUP BY-Statement durchgeführt werden. Die HAVING-Klausel wird verwendet, um Bedingungen auf die aggregierten Ergebnisse anzuwenden, die von den Aggregatfunktionen zurückgegeben werden, die im SELECT-Statement verwendet werden.

Im Gegensatz zum WHERE-Statement, das auf Zeilen vor der Gruppierung angewendet wird, wird die HAVING-Klausel auf die aggregierten Ergebnisse nach der Gruppierung angewendet.

Hier ist ein Beispiel für die Verwendung von HAVING in MySQL:

Angenommen, wir haben eine Tabelle mit dem Namen "orders", die folgende Spalten enthält: 
- "order_id"
- "customer_id"
- "order_date"
- "order_total"
 
Wir möchten den Gesamtwert der Bestellungen für jeden Kunden berechnen und nur die Kunden auswählen, deren Bestellsumme größer als 500 ist.

Wir können dies mit dem *GROUP BY*- und *HAVING*-Statement wie folgt tun:

```sql
SELECT customer_id, SUM(order_total)
FROM orders
GROUP BY customer_id
HAVING SUM(order_total) > 500;
```
In diesem Beispiel werden die Daten in der Tabelle "orders" nach der Spalte "customer_id" gruppiert. Die Funktion "SUM" wird verwendet, um den Gesamtwert der Bestellungen für jede Gruppe zu berechnen. Dann wird das HAVING-Statement verwendet, um nur die Kunden auszuwählen, deren Gesamtbestellsumme größer als 500 ist.

Das Ergebnis der Abfrage wird folgendermaßen aussehen:

|customer_id|SUM(order_total)|
|-----------|----------------|
|1|650.50|
|3|900.25|

Wie man sehen kann, wurden nur die Kunden zurückgegeben, deren Bestellsumme größer als 500 ist. Durch die Verwendung von HAVING in Kombination mit GROUP BY können wir die Ergebnisse unserer Abfragen weiter filtern und verfeinern.

>Das Dokument [Select_having.pdf](select_having.pdf) liefert Ihnen detaillierte Erklärungen und praktische Beispiele aus der [kundenDatenbank](../../10_Auftraege_und_Uebungen/90_Dumps/kundenDatenbank_dump.sql).

**Auftrag**: Setzen Sie den [Auftrag Select having](../../10_Auftraege_und_Uebungen/14_DQL/22_select_having.md) um.

---
Quellen:<br>
https://dev.mysql.com/doc/refman/8.0/en/select.html<br>
https://dev.mysql.com/doc/refman/8.0/en/group-by-modifiers.html