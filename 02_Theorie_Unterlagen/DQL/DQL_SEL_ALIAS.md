# DQL (Data Query Language)

## SELECT ALIAS

Mit dem SELECT-Statement kann man bestimmte Spalten aus einer Tabelle auswählen und anzeigen lassen. Das SELECT-Statement ermöglicht es auch, Aliase für Spalten und Tabellen zu definieren, um die Ergebnisse der Abfrage lesbarer und verständlicher zu machen.

Ein *Alias* ist ein alternativer Name, der für eine Spalte oder eine Tabelle verwendet wird. Man kann Aliase verwenden, um die Namen von Spalten und Tabellen zu ändern oder um längere Namen zu verkürzen. Aliase werden in der SELECT-Liste definiert, indem man nach dem ursprünglichen Spalten- oder Tabellennamen ein "AS" und dann den Aliasnamen angibt.

Hier ist ein Beispiel für die Verwendung von Aliasen in MySQL:

Angenommen, wir haben eine Tabelle mit dem Namen "employees", die folgende Spalten enthält: 
- "employee_id"
- "first_name"
- "last_name"
- "salary"

Wir möchten die Spalten "first_name" und "last_name" anzeigen, aber wir möchten, dass der Name der Spalte "first_name" in "Vorname" geändert wird. Wir möchten auch, dass der Name der Tabelle "employees" in "Mitarbeiter" geändert wird. Wir können dies mit Aliasen wie folgt tun:

```sql
SELECT first_name AS Vorname, last_name, salary
FROM employees AS Mitarbeiter;
```

In diesem Beispiel haben wir Aliasnamen für die Spalte "first_name" und die Tabelle "employees" definiert. Die Spalte "first_name" wird nun als "Vorname" angezeigt, während die Spalte "last_name" und "salary" unverändert bleiben. Die Tabelle "employees" wird nun als "Mitarbeiter" angezeigt.

Das Ergebnis der Abfrage wird folgendermaßen aussehen:

|Vorname|last_name|salary|
|-------|---------|------|
|Max|Mustermann|5000|
|Anna|Schmidt|6000|
|Peter|Müller|5500|

Wie man sehen kann, sind die Aliasnamen für die Spalte "first_name" und die Tabelle "employees" in der Ergebnismenge sichtbar und machen es einfacher, die Ergebnisse der Abfrage zu lesen und zu verstehen

**Auftrag**: Setzen Sie den [Auftrag Select alias](../../10_Auftraege_und_Uebungen/14_DQL/16_select_alias.md) um.

---
Quellen:<br>
https://dev.mysql.com/doc/refman/8.0/en/select.html
