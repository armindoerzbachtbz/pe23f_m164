# DQL (Data Query Language)

## SELECT GROUP BY

In MySQL wird das GROUP BY-Statement verwendet, um Daten in Gruppen zu gruppieren und zusammenzufassen. Es wird normalerweise zusammen mit Aggregatsfunktionen wie *COUNT*, *SUM*, *AVG*, *MIN* und *MAX* verwendet, um Daten in jeder Gruppe zusammenzufassen und die Ergebnisse zu erhalten.

Das GROUP BY-Statement gruppiert Daten basierend auf dem Inhalt einer oder mehrerer Spalten. Das bedeutet, dass alle Zeilen mit demselben Wert in der Gruppierungsspalte in einer Gruppe zusammengefasst werden. Das GROUP BY-Statement gibt dann eine Ergebnismenge zurück, die die Ergebnisse der Aggregatsfunktionen für jede Gruppe enthält.

Hier ist ein Beispiel für die Verwendung des GROUP BY-Statements in MySQL:

Angenommen, wir haben eine Tabelle mit dem Namen "orders", die folgende Spalten enthält: 
- "order_id"
- "customer_id"
- "order_date"
- "order_total"
 
Wir möchten den Gesamtwert der Bestellungen für jeden Kunden berechnen und die Ergebnisse nach Kunden gruppieren.

Wir können dies mit dem GROUP BY-Statement wie folgt tun:

```sql
SELECT customer_id, SUM(order_total)
FROM orders
GROUP BY customer_id;
```
In diesem Beispiel werden die Daten in der Tabelle "orders" nach der Spalte "customer_id" gruppiert. Die Funktion "SUM" wird verwendet, um den Gesamtwert der Bestellungen für jede Gruppe zu berechnen. Das Ergebnis zeigt die Kunden-ID und den Gesamtwert der Bestellungen für jeden Kunden.

Das Ergebnis der Abfrage wird folgendermaßen aussehen:

|customer_id|SUM(order_total)|
|-----------|----------------|
|1|350.50|
|2|210.00|
|3|450.25|

Wie man sehen kann, gibt es eine separate Zeile für jede Kunden-ID, die den Gesamtwert der Bestellungen für diesen Kunden enthält. Durch die Verwendung des GROUP BY-Statements können wir Daten in Gruppen zusammenfassen und aggregieren, um nützliche Informationen aus unseren Tabellen zu extrahieren.

>Das Dokument [Select_group_by.pdf](select_group_by.pdf) liefert Ihnen detaillierte Erklärungen und praktische Beispiele sowie kleine Übungen aus der [kundenDatenbank](../../10_Auftraege_und_Uebungen/90_Dumps/kundenDatenbank_dump.sql).

**Auftrag**: Setzen Sie den [Auftrag Select group by](../../10_Auftraege_und_Uebungen/14_DQL/20_select_group_by.md) um.<br>
**Auftrag**: Setzen Sie den [Auftrag Select group by sort](../../10_Auftraege_und_Uebungen/14_DQL/14_select_groupby_sort.md) um.

---
Quellen: <br>
https://dev.mysql.com/doc/refman/8.0/en/select.html<br>
https://dev.mysql.com/doc/refman/8.0/en/group-by-modifiers.html





